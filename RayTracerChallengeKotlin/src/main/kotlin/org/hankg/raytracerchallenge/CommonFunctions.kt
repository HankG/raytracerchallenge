package org.hankg.raytracerchallenge

import org.hankg.raytracerchallenge.model.Tuple
import org.hankg.raytracerchallenge.model.Vector
import kotlin.math.abs

const val DEFAULT_COMPARISON_TOLERANCE = 1e-12

fun compare(v1:Double, v2:Double, epsilon:Double = DEFAULT_COMPARISON_TOLERANCE):Boolean = abs(v1 - v2) < epsilon

fun Tuple.toVector(): Vector = Vector(this.x, this.y, this.z)
