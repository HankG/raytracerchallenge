package org.hankg.raytracerchallenge.io

import org.hankg.raytracerchallenge.model.Canvas
import org.hankg.raytracerchallenge.model.Color
import java.awt.image.BufferedImage
import java.io.File
import java.nio.file.Path
import javax.imageio.ImageIO

class PngCanvasWriter : CanvasWriter{
    override fun write(filepath: Path, canvas: Canvas) {
        val image = BufferedImage(canvas.width, canvas.height, BufferedImage.TYPE_INT_RGB)
        for(x in 0 until canvas.width) {
            for(y in 0 until canvas.height) {
                image.setRGB(x, y, canvas[x,y].toRGB())
            }
        }

        ImageIO.write(image, "png", filepath.toFile())
    }

    private fun Color.toRGB(): Int = java.awt.Color(this.r.toFloat(), this.g.toFloat(), this.b.toFloat()).rgb
}