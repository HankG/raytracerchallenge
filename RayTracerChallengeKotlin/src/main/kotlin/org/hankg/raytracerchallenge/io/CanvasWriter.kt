package org.hankg.raytracerchallenge.io

import org.hankg.raytracerchallenge.model.Canvas
import java.nio.file.Path

interface CanvasWriter {
    fun write(filepath:Path, canvas:Canvas)
}