package org.hankg.raytracerchallenge.io

import org.hankg.raytracerchallenge.model.Canvas
import org.hankg.raytracerchallenge.model.Color
import java.nio.file.Files
import java.nio.file.Path
import kotlin.math.roundToInt

class PpmCanvasWriter : CanvasWriter{
    val MAX_COLOR_VALUE = 255
    val MAX_LINE_LENGTH = 70


    override fun write(filepath: Path, canvas: Canvas) {
        Files.write(filepath, this.generate(canvas))
    }

    fun generate(canvas: Canvas):List<String> {
        val lines = mutableListOf<String>()
        lines.addAll(generateHeader(canvas))
        for(y in 0 until canvas.height) {
            val stringBuffer = StringBuffer()
            for(x in 0 until canvas.width){
                val pixel = canvas[x, y]
                val redString = colorToPpmString(pixel.r)
                val greenString = colorToPpmString(pixel.g)
                val blueString = colorToPpmString(pixel.b)

                if (stringBuffer.length > MAX_LINE_LENGTH - redString.length) {
                    lines.add(stringBuffer.trim().toString())
                    stringBuffer.setLength(0)
                }
                stringBuffer.append("$redString ")

                if (stringBuffer.length > MAX_LINE_LENGTH - greenString.length) {
                    lines.add(stringBuffer.trim().toString())
                    stringBuffer.setLength(0)
                }
                stringBuffer.append("$greenString ")

                if (stringBuffer.length > MAX_LINE_LENGTH - blueString.length) {
                    lines.add(stringBuffer.trim().toString())
                    stringBuffer.setLength(0)
                }
                stringBuffer.append("$blueString ")
            }
            lines.add(stringBuffer.trim().toString())
        }
        lines.add("\n")
        return lines
    }

    private fun generateHeader(canvas:Canvas): List<String> {
        val version = "P3"
        val dimensions = "${canvas.width} ${canvas.height}"
        val maxColorValue = "$MAX_COLOR_VALUE"
        return listOf(version, dimensions, maxColorValue)
    }

    private fun colorToPpmString(value:Double):String {
        return if (value >= 1.0)
            "255"
        else if (value <= 0.0)
            "0"
        else
            "${(MAX_COLOR_VALUE * value).roundToInt()}"
    }
}