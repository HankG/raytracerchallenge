package org.hankg.raytracerchallenge.MathFunctions

import org.hankg.raytracerchallenge.model.Matrix
import kotlin.math.cos
import kotlin.math.sin

fun identity(size:Int = 4): Matrix {
    val matrix = Matrix(size, size)
    for (i in 0 until size) {
        matrix[i, i] = 1.0
    }

    return matrix
}

fun rotateX(angle:Double): Matrix {
    val matrix = Matrix(4, 4)
    matrix[0, 0] = 1.0
    matrix[1, 1] = cos(angle)
    matrix[1, 2] = -sin(angle)
    matrix[2, 1] = sin(angle)
    matrix[2, 2] = cos(angle)
    matrix[3, 3] = 1.0
    return matrix
}

fun rotateY(angle:Double): Matrix {
    val matrix = Matrix(4, 4)
    matrix[0, 0] = cos(angle)
    matrix[0, 2] = sin(angle)
    matrix[1, 1] = 1.0
    matrix[2, 0] = -sin(angle)
    matrix[2, 2] = cos(angle)
    matrix[3, 3] = 1.0
    return matrix
}

fun rotateZ(angle:Double): Matrix {
    val matrix = Matrix(4, 4)
    matrix[0, 0] = cos(angle)
    matrix[0, 1] = -sin(angle)
    matrix[1, 0] = sin(angle)
    matrix[1, 1] = cos(angle)
    matrix[2, 2] = 1.0
    matrix[3, 3] = 1.0
    return matrix
}

fun scaling(x:Double, y:Double, z:Double): Matrix {
    val matrix = Matrix(4, 4)
    matrix[0, 0] = x
    matrix[1, 1] = y
    matrix[2, 2] = z
    matrix[3, 3] = 1.0
    return matrix
}

fun shearing(x_y:Double, x_z:Double, y_x:Double, y_z:Double, z_x:Double, z_y:Double): Matrix {
    val matrix = identity(4)
    matrix[0, 1] = x_y
    matrix[0, 2] = x_z
    matrix[1, 0] = y_x
    matrix[1, 2] = y_z
    matrix[2, 0] = z_x
    matrix[2, 1] = z_y
    return matrix
}

fun translate(x:Double, y:Double, z:Double): Matrix {
    val matrix = identity(4)
    matrix[0, 3] = x
    matrix[1, 3] = y
    matrix[2, 3] = z
    return matrix
}