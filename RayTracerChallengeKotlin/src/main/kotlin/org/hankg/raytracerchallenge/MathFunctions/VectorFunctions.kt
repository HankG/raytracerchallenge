package org.hankg.raytracerchallenge.MathFunctions

import org.hankg.raytracerchallenge.model.Vector
import org.hankg.raytracerchallenge.toVector

fun reflect(incoming: Vector, normal:Vector):Vector {
    return incoming - (normal * 2.0 * incoming.dot(normal)).toVector()
}