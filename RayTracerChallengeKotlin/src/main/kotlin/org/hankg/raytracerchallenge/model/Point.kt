package org.hankg.raytracerchallenge.model

class Point(x: Double, y: Double, z: Double) : Tuple(x, y, z, 1.0) {
    companion object {
        val ZERO = Point(0.0, 0.0, 0.0)
    }

    operator fun plus(v: Vector): Point {
        val newValue = (this as Tuple) + (v as Tuple)
        return Point(newValue.x, newValue.y, newValue.z)
    }

    operator fun minus(p: Point): Vector {
        val newValue = (this as Tuple) - (p as Tuple)
        return Vector(newValue.x, newValue.y, newValue.z)
    }

    operator fun minus(p: Vector): Point {
        val newValue = (this as Tuple) - (p as Tuple)
        return Point(newValue.x, newValue.y, newValue.z)
    }

    override fun isVector(): Boolean {
        return false
    }

    override fun isPoint(): Boolean {
        return true
    }

    override fun toString(): String {
        return "Point(x=$x, y=$y, z=$z)"
    }

}
