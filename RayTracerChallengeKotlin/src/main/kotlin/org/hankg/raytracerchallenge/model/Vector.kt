package org.hankg.raytracerchallenge.model

import kotlin.math.sqrt


class Vector(x: Double, y: Double, z: Double) : Tuple(x, y, z, 0.0) {
    companion object {
        val ZERO = Vector(0.0, 0.0, 0.0)
    }

    operator fun plus(v: Vector): Vector {
        val newValue = (this as Tuple) + (v as Tuple)
        return Vector(newValue.x, newValue.y, newValue.z)
    }

    operator fun minus(v: Vector): Vector {
        val newValue = (this as Tuple) - (v as Tuple)
        return Vector(newValue.x, newValue.y, newValue.z)
    }

    fun cross(other: Vector): Vector {
        val x = (this.y * other.z) - (this.z * other.y)
        val y = (this.z * other.x) - (this.x * other.z)
        val z = (this.x * other.y) - (this.y * other.x)
        return Vector(x, y, z)
    }

    fun dot(other: Vector): Double {
        return (this.x * other.x) +
                (this.y * other.y) +
                (this.z * other.z)
    }

    override fun isVector(): Boolean {
        return true
    }

    override fun isPoint(): Boolean {
        return false
    }

    fun magnitude():Double {
        return sqrt((x * x) + (y * y) + (z * z))
    }

    fun normalize(): Vector {
        val scale = this.magnitude()
        return Vector(x / scale, y / scale, z / scale)
    }

    override fun toString(): String {
        return "Vector(x=$x, y=$y, z=$z)"
    }

}