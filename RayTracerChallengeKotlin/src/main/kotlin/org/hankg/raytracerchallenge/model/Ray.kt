package org.hankg.raytracerchallenge.model

import org.hankg.raytracerchallenge.toVector

data class Ray (val origin:Point = Point.ZERO,val direction:Vector = Vector.ZERO) {

    fun position(t:Double): Point {
        return origin + (direction * t).toVector()
    }

    fun transform(matrix:Matrix): Ray {
        return Ray(matrix * origin, matrix * direction)
    }
}