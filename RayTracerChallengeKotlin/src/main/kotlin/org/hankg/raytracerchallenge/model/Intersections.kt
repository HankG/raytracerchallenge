package org.hankg.raytracerchallenge.model

class Intersections() {
    companion object {
        val NO_INTERSECTION = Intersection()

        fun fromRaySphere(ray:Ray, obj:Sphere): Intersections {
            val intersections = Intersection.intersections(ray, obj)
            val newIntersections = Intersections()
            newIntersections.addAll(intersections)
            return newIntersections
        }
    }
    private val items = mutableListOf<Intersection>()

    val count: Int
        get() = items.count()

    operator fun get(i:Int):Intersection = items[i]

    fun add(intersection:Intersection) = items.add(intersection)

    fun addAll(intersections: Collection<Intersection>) = items.addAll(intersections)

    fun hit(): Intersection {
        val sortedFilteredItems = items.filter {it.t >= 0}.sortedBy { it.t }
        if (sortedFilteredItems.isEmpty()) return NO_INTERSECTION
        return sortedFilteredItems.first()
    }

}