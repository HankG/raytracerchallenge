package org.hankg.raytracerchallenge.model

import org.hankg.raytracerchallenge.DEFAULT_COMPARISON_TOLERANCE

open class Tuple(val x:Double, val y:Double, val z:Double, val w:Double) {
    companion object {
        val ZERO = Tuple(0.0, 0.0, 0.0, 0.0)
    }

    open fun isVector(): Boolean {
        return org.hankg.raytracerchallenge.compare(w, 0.0)
    }

    open fun isPoint(): Boolean {
        return org.hankg.raytracerchallenge.compare(w, 1.0)
    }

    operator fun plus(other: Tuple): Tuple {
        return Tuple(x + other.x, y + other.y, z + other.z, w + other.w)
    }

    operator fun minus(other: Tuple): Tuple {
        return Tuple(x - other.x, y - other.y, z - other.z, w - other.w)
    }

    operator fun unaryMinus(): Tuple {
        return Tuple(-x, -y, -z, -w)
    }

    operator fun times(a:Double): Tuple {
        return Tuple(x * a, y * a, z * a, w * a)
    }

    operator fun div(a:Double): Tuple {
        return Tuple(x / a, y / a, z / a, w / a)
    }

    fun compare(other: Tuple, tolerance:Double = DEFAULT_COMPARISON_TOLERANCE): Boolean {
        if (!org.hankg.raytracerchallenge.compare(x, other.x, tolerance)) return false
        if (!org.hankg.raytracerchallenge.compare(y, other.y, tolerance)) return false
        if (!org.hankg.raytracerchallenge.compare(z, other.z, tolerance)) return false
        if (!org.hankg.raytracerchallenge.compare(w, other.w, tolerance)) return false

        return true
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as Tuple

        if (x != other.x) return false
        if (y != other.y) return false
        if (z != other.z) return false
        if (w != other.w) return false

        return true
    }

    override fun hashCode(): Int {
        var result = x.hashCode()
        result = 31 * result + y.hashCode()
        result = 31 * result + z.hashCode()
        result = 31 * result + w.hashCode()
        return result
    }

    override fun toString(): String {
        return "Tuple(x=$x, y=$y, z=$z, w=$w)"
    }

}
