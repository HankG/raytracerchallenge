package org.hankg.raytracerchallenge.model

import org.hankg.raytracerchallenge.DEFAULT_COMPARISON_TOLERANCE
import org.hankg.raytracerchallenge.MathFunctions.identity
import kotlin.IllegalArgumentException
import kotlin.math.abs

data class Matrix(val width:Int, val height:Int) {
    companion object {
        val ZERO = Matrix(1, 1)
        val I3 = identity(3)
        val I4 = identity(4)
    }
    private val value: Array<Array<Double>> = Array(height) { Array(width) { 0.0 } }

    operator fun get(row:Int, column:Int) = value[row][column]

    operator fun set(row:Int, column:Int, v:Double) {
        value[row][column] = v
    }

    operator fun times(other:Matrix): Matrix {
        if (this.width != other.height) {
            throw IllegalArgumentException("A's width and B's height must be the same: ${this.width} != ${other.width}")
        }


        val newMatrix = Matrix(other.width, this.height)
        for (row in 0 until newMatrix.height) {
            for (column in 0 until newMatrix.width) {
                var sum = 0.0
                for (i in 0 until this.width) {
                    sum += this[row, i] * other[i, column]
                }
                newMatrix[row, column] = sum
            }
        }

        return newMatrix
    }

    operator fun times(tuple:Tuple): Tuple {
        if (this.width != 4 || this.height != 4) {
            throw IllegalArgumentException("Can only multiply 4x4 matrices")
        }

        val x = this[0, 0] * tuple.x + this[0, 1] * tuple.y + this[0, 2] * tuple.z + this[0, 3] * tuple.w
        val y = this[1, 0] * tuple.x + this[1, 1] * tuple.y + this[1, 2] * tuple.z + this[1, 3] * tuple.w
        val z = this[2, 0] * tuple.x + this[2, 1] * tuple.y + this[2, 2] * tuple.z + this[2, 3] * tuple.w
        val w = this[3, 0] * tuple.x + this[3, 1] * tuple.y + this[3, 2] * tuple.z + this[3, 3] * tuple.w

        return Tuple(x, y, z, w)
    }

    operator fun times(point:Point): Point {
        val result = times(point as Tuple)
        return Point(result.x, result.y, result.z)
    }

    operator fun times(vector:Vector): Vector {
        val result = times(vector as Tuple)
        return Vector(result.x, result.y, result.z)
    }

    fun cofactor(row:Int, column:Int): Double{
        if ((row + column) % 2 == 0) {
            return this.minor(row, column)
        }

        return -this.minor(row, column)
    }


    fun compare(other: Matrix, tolerance:Double = DEFAULT_COMPARISON_TOLERANCE): Boolean {
        for (row in 0 until height) {
            for (column in 0 until width) {
                if(!org.hankg.raytracerchallenge.compare(this[row, column], other[row, column], tolerance)) return false
            }
        }

        return true
    }

    fun determinant(): Double {
        if (this.width == 2 &&  this.height == 2) {
            return (this[0,0] * this [1,1]) - (this[0,1] * this[1,0])
        }

        var determinant = 0.0
        for (column in 0 until width) {
            determinant += this[0, column] * cofactor(0, column)
        }

        return determinant
    }

    fun invert(): Matrix {
        val determinant = this.determinant()
        if (!isInvertable(determinant)) throw IllegalArgumentException("Matrix not invertable")

        val newMatrix = Matrix(width, height)

        for (row in 0 until height) {
            for (col in 0 until width) {
                val elementCofactor = cofactor(row, col)
                // swapped elements captures transpose operation implicitly
                newMatrix[col, row] = elementCofactor / determinant
            }
        }

        return newMatrix
    }

    fun isInvertable(): Boolean = isInvertable(determinant())

    fun minor(row:Int, column:Int): Double{
        val subMatrix = this.subMatrix(row, column)
        return subMatrix.determinant()
    }

    fun subMatrix(rowToRemove:Int, columnToRemove:Int): Matrix {
        val newMatrix = Matrix(width - 1, height - 1)
        var newRowIndex = 0
        for (row in 0 until height) {
            if (row == rowToRemove) {
                continue
            }

            var newColumnIndex = 0
            for (column in 0 until width) {
                if (column == columnToRemove) {
                    continue
                }
                newMatrix[newRowIndex, newColumnIndex] = this[row, column]
                newColumnIndex++
            }
            newRowIndex++
        }

        return newMatrix
    }

    fun transpose() : Matrix {
        val newMatrix = Matrix(height, width)
        for (row in 0 until height) {
            for (column in 0 until width) {
                newMatrix[row, column] = this[column, row]
            }
        }

        return newMatrix
    }

    override fun toString(): String {
        val buffer = StringBuffer()
        for (row in 0 until height) {
            for (column in 0 until width) {
                buffer.append("${this[row, column]}, ")
            }
            buffer.append(System.lineSeparator())
        }

        return buffer.toString()
    }

    private fun isInvertable(determinant:Double) = abs(determinant) > DEFAULT_COMPARISON_TOLERANCE

}