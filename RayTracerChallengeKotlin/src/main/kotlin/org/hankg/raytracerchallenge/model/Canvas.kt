package org.hankg.raytracerchallenge.model

data class Canvas(val width:Int, val height:Int) {
    private val pixels: Array<Array<Color>> = Array(width) {Array(height) { Color.BLACK} }

    operator fun get(x:Int, y:Int) = pixels[x][y]

    operator fun set(x:Int, y:Int, color:Color) {
        pixels[x][y] = color
    }

    operator fun set(point:Point, color:Color) {
        this[point.x.toInt(), point.y.toInt()] = color
    }

    /**
     * Fills the entire canvas with the given color.
     */
    fun fillCanvas(color:Color) {
        for(x in 0 until width) {
            for (y in 0 until height) {
                pixels[x][y] = color
            }
        }
    }
}
