package org.hankg.raytracerchallenge.model

data class Sphere(var transform:Matrix = Matrix.I4) {

    val position:Point = Point.ZERO

    fun normal(worldPoint:Point) : Vector {
        val inverseTransform = transform.invert()
        val objectPoint = inverseTransform * worldPoint
        val objectNormal = objectPoint - position
        val worldNormal = inverseTransform.transpose() * objectNormal
        return worldNormal.normalize()
    }

}