package org.hankg.raytracerchallenge.model

import kotlin.math.sqrt

data class Intersection(val t:Double = 0.0, val item:Sphere = Sphere()) {
    companion object {
        fun intersections(ray:Ray, obj:Sphere):List<Intersection> {
            val morphedRay = ray.transform(obj.transform.invert())
            val sphereToRay = morphedRay.origin - obj.position
            val a = morphedRay.direction.dot(morphedRay.direction)
            val b = 2 * morphedRay.direction.dot(sphereToRay)
            val c = sphereToRay.dot(sphereToRay) - 1
            val discriminant = (b * b) - (4 * a * c)

            if (discriminant < 0) {
                return listOf()
            }

            val t1 = (-b - sqrt(discriminant)) / (2 * a)
            val t2 = (-b + sqrt(discriminant)) / (2 * a)
            return listOf(
                    Intersection(t1, obj),
                    Intersection(t2, obj)
            )
        }
    }
}