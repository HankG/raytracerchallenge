package org.hankg.raytracerchallenge.model

import org.hankg.raytracerchallenge.DEFAULT_COMPARISON_TOLERANCE

data class Color(val r:Double, val g:Double, val b:Double) {
    companion object {
        val BLACK = Color(0.0, 0.0, 0.0)
        val BLUE = Color(0.0, 0.0, 1.0)
        val GREEN = Color(0.0, 1.0, 0.0)
        val RED = Color(1.0, 0.0, 0.0)
        val WHITE = Color(1.0, 1.0, 1.0)
    }

    operator fun plus(other: Color): Color {
        return Color(r + other.r, g + other.g, b + other.b)
    }

    operator fun minus(other: Color): Color {
        return Color(r - other.r, g - other.g, b - other.b)
    }

    operator fun times(other: Color): Color {
        return Color(
                r * other.r,
                g * other.g,
                b * other.b
        )
    }

    operator fun times(scale:Double): Color {
        return Color(r * scale, g * scale, b * scale)
    }

    fun compare(other: Color, tolerance:Double = DEFAULT_COMPARISON_TOLERANCE): Boolean {
        if (!org.hankg.raytracerchallenge.compare(r, other.r, tolerance)) return false
        if (!org.hankg.raytracerchallenge.compare(g, other.g, tolerance)) return false
        if (!org.hankg.raytracerchallenge.compare(b, other.b, tolerance)) return false

        return true
    }

}