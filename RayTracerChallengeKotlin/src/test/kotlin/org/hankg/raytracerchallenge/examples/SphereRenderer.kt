package org.hankg.raytracerchallenge.examples

import org.hankg.raytracerchallenge.MathFunctions.rotateZ
import org.hankg.raytracerchallenge.MathFunctions.scaling
import org.hankg.raytracerchallenge.MathFunctions.shearing
import org.hankg.raytracerchallenge.MathFunctions.translate
import org.hankg.raytracerchallenge.io.PngCanvasWriter
import org.hankg.raytracerchallenge.model.*
import java.nio.file.Paths

fun main(args:Array<String>) {
    val outputPath = Paths.get("/tmp/SphereSilhouette.png")
    val canvas = Canvas(200, 200)
    canvas.fillCanvas(Color.BLUE)

    // Scene Setup
    val sphere = Sphere()
    sphere.transform =
            shearing(0.5, 0.0, 0.75, 0.0, 0.0, 0.0) *
            rotateZ(Math.toRadians(45.0)) *
            scaling(0.75, 0.5, 1.0) *
            translate(0.5, -0.25, 0.0)
    val rayOrigin = Point(0.0, 0.0, -5.0)
    val wallZPosition = 5.0

    // Renderer Setup
    val wallSize = 7.0
    val wallHalfSize = wallSize / 2
    val pixelSize = wallSize / canvas.width

    for (y in 0 until canvas.height) {
        val worldY = wallHalfSize - pixelSize * y
        for( x in 0 until canvas.width) {
            val worldX = pixelSize * x - wallHalfSize
            val position = Point(worldX, worldY, wallZPosition)
            val rayVector = (position - rayOrigin).normalize()
            val ray = Ray(rayOrigin, rayVector)
            val intersections = Intersections.fromRaySphere(ray, sphere)
            val hit = intersections.hit()
            if (hit != Intersections.NO_INTERSECTION) {
                canvas[x, y] = Color.RED
            }
        }
    }
    PngCanvasWriter().write(outputPath, canvas)

}