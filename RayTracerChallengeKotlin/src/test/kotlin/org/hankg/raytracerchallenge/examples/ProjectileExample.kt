package org.hankg.raytracerchallenge.examples

import org.hankg.raytracerchallenge.io.PngCanvasWriter
import org.hankg.raytracerchallenge.io.PpmCanvasWriter
import org.hankg.raytracerchallenge.model.Canvas
import org.hankg.raytracerchallenge.model.Color
import org.hankg.raytracerchallenge.model.Point
import org.hankg.raytracerchallenge.model.Vector
import org.hankg.raytracerchallenge.toVector
import java.awt.image.BufferedImage
import java.awt.image.DirectColorModel
import java.io.File
import java.nio.file.Paths
import javax.imageio.ImageIO
import kotlin.math.floor

data class Projectile(val position: Point, val velocity: Vector)
val gravity = Vector(0.0, -0.1, 0.0)
val wind = Vector(-0.01, 0.0, 0.0)

fun tick(p: Projectile): Projectile {
    val newPosition = p.position + p.velocity
    val newVelocity = p.velocity + gravity + wind
    return Projectile(newPosition, newVelocity)
}

fun main(args:Array<String>){
    val width = 900
    val height = 500
    val path = Paths.get("/tmp/projection.png")
    val initialPosition = Point(0.0, 1.0, 0.0)
    val initialVelocity = Vector(1.0, 1.8, 0.0).normalize() * 11.25
    val canvas = Canvas(width, height)
    var projectile = Projectile(initialPosition, initialVelocity.toVector())
    println(projectile)

    for (i in 0..1000) {
        projectile = tick(projectile)
        println("$i - $projectile")
        val x = projectile.position.x.toInt()
        val y = height - projectile.position.y.toInt()

        if (x in 0 until width && y in 0 until height) {
            canvas[x, y] = Color.BLUE
        }

        if (projectile.position.y <= 0) {
            println("Hit ground")
            break
        }
    }

    PngCanvasWriter().write(path, canvas)
}