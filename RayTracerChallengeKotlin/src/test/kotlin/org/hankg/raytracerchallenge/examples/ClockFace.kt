package org.hankg.raytracerchallenge.examples

import org.hankg.raytracerchallenge.MathFunctions.rotateZ
import org.hankg.raytracerchallenge.io.PngCanvasWriter
import org.hankg.raytracerchallenge.model.Canvas
import org.hankg.raytracerchallenge.model.Color
import org.hankg.raytracerchallenge.model.Point
import org.hankg.raytracerchallenge.model.Vector
import java.nio.file.Paths

fun main(args:Array<String>) {
    val outputPath = Paths.get("/tmp/clockface.png")
    val canvas = Canvas(200, 200)
    val proportion = 0.8
    val clockRadius = (canvas.width / 2 * proportion)
    val xOrigin = canvas.width / 2.0
    val yOrigin = canvas.height / 2.0
    val origin = Point(xOrigin, yOrigin, 0.0)
    val noonVector = Vector(0.0, -clockRadius, 0.0)

    canvas.fillCanvas(Color.BLUE)
    for (hour in 0 until 360 step 30) {
        val hourAngle = Math.toRadians(hour.toDouble())
        val hourRotation = rotateZ(hourAngle)
        val hourVector = hourRotation * noonVector
        val point = origin.plus(hourVector)
        canvas[point] = Color.WHITE
    }

    PngCanvasWriter().write(outputPath, canvas)


}