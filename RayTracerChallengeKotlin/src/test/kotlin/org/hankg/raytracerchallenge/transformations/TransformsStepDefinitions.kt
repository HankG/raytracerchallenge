package org.hankg.raytracerchallenge.transformations

import io.cucumber.java.en.Given
import io.cucumber.java.en.Then
import io.cucumber.java.en.When
import org.hankg.raytracerchallenge.MathFunctions.*
import org.hankg.raytracerchallenge.model.Matrix
import org.hankg.raytracerchallenge.model.Point
import org.hankg.raytracerchallenge.model.Tuple
import org.hankg.raytracerchallenge.model.Vector
import org.hankg.raytracerchallenge.tuples.point
import org.hankg.raytracerchallenge.tuples.point2
import org.hankg.raytracerchallenge.tuples.vector
import org.junit.Assert
import kotlin.math.PI


var transform = Matrix.ZERO
var inv = Matrix.ZERO
var half_quarter = Matrix.ZERO
var full_quarter = Matrix.ZERO
var A = Matrix.ZERO
var B = Matrix.ZERO
var C = Matrix.ZERO
var T = Matrix.ZERO
var point3 = Point.ZERO
var point4 = Point.ZERO

@Given("transform ← translation\\({double}, {double}, {double})")
fun givenTranslationTransform(x:Double, y:Double, z:Double) {
    transform = translate(x, y, z)
}

@Given("transform ← scaling\\({double}, {double}, {double})")
fun givenScalingTransform(x:Double, y:Double, z:Double) {
    transform = scaling(x, y, z)
}

@Then("transform * p = point\\({double}, {double}, {double})")
fun checkTransformOnPoint(x:Double, y:Double, z:Double) {
    val expected = Point(x, y, z)
    val actual = transform * point
    Assert.assertTrue(expected.compare(actual))
}

@Given("inv ← inverse\\(transform)")
fun givenInverseOfTransform() {
    inv = transform.invert()
}

@Then("inv * p = point\\({double}, {double}, {double})")
fun checkTransformInverseOnPoint(x:Double, y:Double, z:Double) {
    val expected = Point(x, y, z)
    val actual = inv * point
    Assert.assertTrue(expected.compare(actual))
}

@Then("transform * v = v")
fun checkTransformOnVector() {
    val expected = vector
    val actual = inv * expected
    Assert.assertTrue(expected.compare(actual))
}

@Then("transform * v = vector\\({double}, {double}, {double})")
fun checkTransformOnVector(x:Double, y:Double, z:Double) {
    val expected = Vector(x, y, z) as Tuple
    val actual = transform * vector
    Assert.assertTrue(expected.compare(actual))
}

@Then("inv * v = vector\\({double}, {double}, {double})")
fun checkTransformInverseOnVector(x:Double, y:Double, z:Double) {
    val expected = Vector(x, y, z)
    val actual = inv * vector
    Assert.assertTrue(expected.compare(actual))
}

@Given("half_quarter ← rotation_x\\(π \\/ {double})")
fun givenHalfQuarterXRotation(value:Double) {
    half_quarter = rotateX(PI / value)
}

@Given("full_quarter ← rotation_x\\(π \\/ {double})")
fun givenFullQuarterXRotation(value:Double) {
    full_quarter = rotateX(PI / value)
}

@Given("half_quarter ← rotation_y\\(π \\/ {double})")
fun givenHalfQuarterYRotation(value:Double) {
    half_quarter = rotateY(PI / value)
}

@Given("full_quarter ← rotation_y\\(π \\/ {double})")
fun givenFullQuarterYRotation(value:Double) {
    full_quarter = rotateY(PI / value)
}

@Given("half_quarter ← rotation_z\\(π \\/ {double})")
fun givenHalfQuarterZRotation(value:Double) {
    half_quarter = rotateZ(PI / value)
}

@Given("full_quarter ← rotation_z\\(π \\/ {double})")
fun givenFullQuarterZRotation(value:Double) {
    full_quarter = rotateZ(PI / value)
}

@Then("half_quarter * p = point\\({double}, {double}, {double})")
fun checkHalfQuarterRotationPointTransform(x:Double, y:Double, z:Double) {
    val expected = Point(x, y, z)
    val actual = half_quarter * point
    Assert.assertTrue(expected.compare(actual))
}

@Then("full_quarter * p = point\\({double}, {double}, {double})")
fun checkFullQuarterRotationPointTransform(x:Double, y:Double, z:Double) {
    val expected = Point(x, y, z)
    val actual = full_quarter * point
    Assert.assertTrue(expected.compare(actual))
}

@Given("inv ← inverse\\(half_quarter)")
fun givenInverseHalfQuarter() {
    inv = half_quarter.invert()
}

@Given("transform ← shearing\\({double}, {double}, {double}, {double}, {double}, {double})")
fun givenShearing(x_y:Double, x_z:Double, y_x:Double, y_z:Double, z_x:Double, z_y:Double) {
    transform = shearing(x_y, x_z, y_x, y_z, z_x, z_y)
}

@Given("A ← rotation_x\\(π \\/ {double})")
fun givenARotationX(value:Double) {
    A = rotateX(PI/value)
}

@Given("B ← scaling\\({double}, {double}, {double})")
fun givenBScaling(x:Double, y:Double, z:Double) {
    B = scaling(x, y, z)
}

@Given("C ← translation\\({double}, {double}, {double})")
fun givenCTranslate(x:Double, y:Double, z:Double) {
    C = translate(x, y, z)
}

@When("p2 ← A * p")
fun whenP2IsATimesP() {
    point2 = A * point
}

@When("p3 ← B * p2")
fun whenP3IsBTimesP() {
    point3 = B * point2
}

@When("p4 ← C * p3")
fun whenP4IsBTimesP() {
    point4 = C * point3
}

@Then("p2 = point\\({int}, {int}, {int})")
fun checkP2Equals(x:Double, y:Double, z:Double) {
    val expected = Point(x, y, z)
    Assert.assertTrue(expected.compare(point2))
}

@Then("p3 = point\\({int}, {int}, {int})")
fun checkP3Equals(x:Double, y:Double, z:Double) {
    val expected = Point(x, y, z)
    Assert.assertTrue(expected.compare(point3))
}

@Then("p4 = point\\({int}, {int}, {int})")
fun checkP4Equals(x:Double, y:Double, z:Double) {
    val expected = Point(x, y, z)
    Assert.assertTrue(expected.compare(point4))
}

@When("T ← C * B * A")
fun setTFromABC() {
    T = C * B * A
}

@Then("T * p = point\\({int}, {int}, {int})")
fun checkTpEquals(x:Double, y:Double, z:Double) {
    val expected = Point(x, y, z)
    val actual = T * point
    Assert.assertTrue(expected.compare(actual))
}