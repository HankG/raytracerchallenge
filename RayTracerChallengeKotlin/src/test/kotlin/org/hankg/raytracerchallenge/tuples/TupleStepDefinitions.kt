package org.hankg.raytracerchallenge.tuples

import io.cucumber.java.en.And
import io.cucumber.java.en.Given
import io.cucumber.java.en.Then
import io.cucumber.java.en.When
import junit.framework.Assert.assertTrue
import org.hankg.raytracerchallenge.MathFunctions.reflect
import org.hankg.raytracerchallenge.model.*
import org.junit.Assert
import kotlin.math.sqrt


val epsilon = 1e-12
var color: Color = Color.BLACK
var color1: Color = Color.BLACK
var color2: Color = Color.BLACK
var color3: Color = Color.BLACK
var norm: Vector = Vector.ZERO
var surfaceNormal: Vector = Vector.ZERO
var point: Point = Point.ZERO
var point1: Point = Point.ZERO
var point2: Point = Point.ZERO
var reflection: Vector = Vector.ZERO
var tuple: Tuple = Tuple.ZERO
var tuple1: Tuple = Tuple.ZERO
var tuple2: Tuple = Tuple.ZERO
var vector: Vector = Vector.ZERO
var vector1: Vector = Vector.ZERO
var vector2: Vector = Vector.ZERO
var zeroVector: Vector = Vector.ZERO

// *********************************************************
// Scenario: A tuple with w=1.0 is a point
// Scenario: A tuple with w=0 is a vector
@Given("a ← tuple\\({double}, {double}, {double}, {double})")
fun givenPointTuple(x:Double, y:Double, z:Double, w:Double) {
    tuple = Tuple(x, y, z, w)
}

@Then("a.x = {double}")
fun thenAx(expectedX:Double) {
    Assert.assertEquals(expectedX, tuple.x, epsilon)
}

@And("a.y = {double}")
fun thenAy(expectedY:Double) {
    Assert.assertEquals(expectedY, tuple.y, epsilon)
}

@And("a.z = {double}")
fun thenAz(expectedZ:Double) {
    Assert.assertEquals(expectedZ, tuple.z, epsilon)
}

@And("a.w = {double}")
fun thenAw(expectedW:Double) {
    Assert.assertEquals(expectedW, tuple.w, epsilon)
}

@And("a is a point")
fun thenIsPoint() {
    Assert.assertTrue(tuple.isPoint())
}

@And("a is not a point")
fun thenIsNotPoint() {
    Assert.assertFalse(tuple.isPoint())
}

@And("a is a vector")
fun thenAIsVector() {
    Assert.assertTrue(tuple.isVector())
}

@And("a is not a vector")
fun thenAIsNotVector() {
    Assert.assertFalse(tuple.isVector())
}

// *********************************************************
// point() creates tuples with w=1
// vector() creates tuples with w=0
@Given("p ← point\\({double}, {double}, {double})")
fun givenPoint(x:Double, y:Double, z:Double) {
    point = Point(x, y, z)
}

@Then("p = tuple\\({double}, {double}, {double}, {double})")
fun thenPointEqualsTuple(x:Double, y:Double, z:Double, w:Double) {
    val expected = Tuple(x, y, z, w)
    Assert.assertTrue(expected.compare(point, epsilon))
}

@Given("v ← vector\\({double}, {double}, {double})")
fun givenVector(x:Double, y:Double, z:Double) {
    vector = Vector(x, y, z)
}

@Then("v = tuple\\({double}, {double}, {double}, {double})")
fun thenVectorEqualsTuple(x:Double, y:Double, z:Double, w:Double) {
    val expected = Tuple(x, y, z, w)
    Assert.assertTrue(expected.compare(vector, epsilon))
}


// *********************************************************
// Adding two tuples
// Subtracting two points
// Subtracting a vector from a point
@Given("a1 ← tuple\\({double}, {double}, {double}, {double})")
fun givenATuple(x:Double, y:Double, z:Double, w:Double) {
    tuple1 = Tuple(x, y, z, w)
}

@Given("a2 ← tuple\\({double}, {double}, {double}, {double})")
fun givenSecondTuple(x:Double, y:Double, z:Double, w:Double) {
    tuple2 = Tuple(x, y, z, w)
}

@Then("a1 + a2 = tuple\\({double}, {double}, {double}, {double})")
fun testTupleAdd(x:Double, y:Double, z:Double, w:Double) {
    val expected = Tuple(x, y, z, w)
    val actual = tuple1 + tuple2
    Assert.assertTrue(expected.compare(actual))
}

@Given("p1 ← point\\({double}, {double}, {double})")
fun givenFirstPoint(x:Double, y:Double, z:Double) {
    point1 = Point(x, y, z)
}

@And("p2 ← point\\({double}, {double}, {double})")
fun givenSecondPoint(x:Double, y:Double, z:Double) {
    point2 = Point(x, y, z)
}

@Then("p1 - p2 = vector\\({double}, {double}, {double})")
fun testPointSubtraction(x:Double, y:Double, z:Double) {
    val expected = Vector(x, y, z)
    val actual = point1 - point2
    Assert.assertTrue(expected.compare(actual))
}

@Then("p - v = point\\({double}, {double}, {double})")
fun testPointMinusVector(x:Double, y:Double, z:Double){
    val expected = Point(x, y, z)
    val actual = point - vector
    Assert.assertTrue(expected.compare(actual))
}

// *********************************************************
// Subtracting two vectors
@Given("v1 ← vector\\({double}, {double}, {double})")
fun givenFirstVector(x:Double, y:Double, z:Double) {
    vector1 = Vector(x, y, z)
}

@And("v2 ← vector\\({double}, {double}, {double})")
fun givenSecondVector(x:Double, y:Double, z:Double) {
    vector2 = Vector(x, y, z)
}

@Then("v1 - v2 = vector\\({double}, {double}, {double})")
fun testVectorMinusVector(x:Double, y:Double, z:Double){
    val expected = Vector(x, y, z)
    val actual = vector1 - vector2
    Assert.assertTrue(expected.compare(actual))
}

// *********************************************************
// Subtracting a vector from the zero vector
@Given("zero ← vector\\({double}, {double}, {double})")
fun givenZeroVector(x:Double, y:Double, z:Double) {
    zeroVector = Vector(x, y, z)
}

@Then("zero - v = vector\\({double}, {double}, {double})")
fun testZeroVectorMinusVector(x:Double, y:Double, z:Double){
    val expected = Vector(x, y, z)
    val actual = zeroVector - vector
    Assert.assertTrue(expected.compare(actual))
}

// *********************************************************
// Negating a tuple
@Then("-a = tuple\\({double}, {double}, {double}, {double})")
fun testNegativeTuple(x:Double, y:Double, z:Double, w:Double){
    val expected = Tuple(x, y, z, w)
    val actual = -tuple
    Assert.assertTrue(expected.compare(actual))
}

// *********************************************************
// Multiply a tuple
@Then("a * {double} = tuple\\({double}, {double}, {double}, {double})")
fun testMultiplyTuple(a:Double, x:Double, y:Double, z:Double, w:Double){
    val expected = Tuple(x, y, z, w)
    val actual = tuple * a
    Assert.assertTrue(expected.compare(actual))
}

// *********************************************************
// Negating a tuple
@Then("a / {double} = tuple\\({double}, {double}, {double}, {double})")
fun testDivideTuple(a:Double, x:Double, y:Double, z:Double, w:Double){
    val expected = Tuple(x, y, z, w)
    val actual = tuple / a
    Assert.assertTrue(expected.compare(actual))
}

// *********************************************************
// Vector magnitude
@Then("magnitude\\(v) = {double}")
fun testSimpleMagnitude(m:Double){
    Assert.assertEquals(vector.magnitude(), m, epsilon)
}

@Then("magnitude\\(v) = √{double}")
fun testComplexMagnitude(m:Double){
    Assert.assertEquals(vector.magnitude(), sqrt(m), epsilon)
}

// *********************************************************
// Vector normalization
@Then("normalize\\(v) = vector\\({double}, {double}, {double})")
fun testNormalization(x:Double, y:Double, z:Double) {
    val expected = Vector(x, y, z)
    val actual = vector.normalize()
    Assert.assertTrue(expected.compare(actual))
}

@Then("normalize\\(v) = approximately vector\\({double}, {double}, {double})")
fun testNormalizationApprox(x:Double, y:Double, z:Double) {
    val tolerance = 1e-5
    val expected = Vector(x, y, z)
    val actual = vector.normalize()
    Assert.assertTrue(expected.compare(actual, tolerance))
}

@When("norm ← normalize\\(v)")
fun normalizeVector() {
    norm = vector.normalize()
}

@Then("magnitude\\(norm) = {double}")
fun magnitude_norm(magnitude:Double) {
    Assert.assertEquals(norm.magnitude(), magnitude, epsilon)
}

// *********************************************************
// Vector and dot product tests
@Given("a ← vector\\({double}, {double}, {double})")
fun givenAVector(x:Double, y:Double, z:Double) {
    vector1 = Vector(x, y, z)
}

@Given("b ← vector\\({double}, {double}, {double})")
fun givenBVector(x:Double, y:Double, z:Double) {
    vector2 = Vector(x, y, z)
}

@Then("dot\\(a, b) = {double}")
fun testDotProduct(expected:Double) {
    Assert.assertEquals(expected, vector1.dot(vector2), epsilon)
}

@Then("cross\\(a, b) = vector\\({double}, {double}, {double})")
fun testAcrossB(x:Double, y:Double, z:Double) {
    val expected = Vector(x, y, z)
    val actual = vector1.cross(vector2)
    Assert.assertTrue(expected.compare(actual))
}

@Then("cross\\(b, a) = vector\\({int}, {int}, {int})")
fun testBcrossA(x:Double, y:Double, z:Double) {
    val expected = Vector(x, y, z)
    val actual = vector2.cross(vector1)
    Assert.assertTrue(expected.compare(actual))
}

// *********************************************************
// Colors
@Given("c ← color\\({double}, {double}, {double})")
fun givenColor(r:Double, g:Double, b:Double) {
    color = Color(r, g, b)
}

@Given("c1 ← color\\({double}, {double}, {double})")
fun givenColor1(r:Double, g:Double, b:Double) {
    color1 = Color(r, g, b)
}

@Given("c2 ← color\\({double}, {double}, {double})")
fun givenColor2(r:Double, g:Double, b:Double) {
    color2 = Color(r, g, b)
}

@Given("c3 ← color\\({double}, {double}, {double})")
fun givenColor3(r:Double, g:Double, b:Double) {
    color3 = Color(r, g, b)
}

@Then("c.red = {double}")
fun checkColorRed(expected:Double) {
    Assert.assertEquals(expected, color.r, epsilon)
}

@Then("c.green = {double}")
fun checkColorGreen(expected:Double) {
    Assert.assertEquals(expected, color.g, epsilon)
}

@Then("c.blue = {double}")
fun checkColorBlue(expected:Double) {
    Assert.assertEquals(expected, color.b, epsilon)
}

@Then("c1 + c2 = color\\({double}, {double}, {double})")
fun checkColorAddition(r:Double, g:Double, b:Double) {
    val expected = Color(r, g, b)
    val actual = color1 + color2
    Assert.assertTrue(expected.compare(actual))
}

@Then("c1 - c2 = color\\({double}, {double}, {double})")
fun checkColorSubtraction(r:Double, g:Double, b:Double) {
    val expected = Color(r, g, b)
    val actual = color1 - color2
    Assert.assertTrue(expected.compare(actual))
}

@Then("c * {double} = color\\({double}, {double}, {double})")
fun checkColorScalarMultiply(scale:Double, r:Double, g:Double, b:Double) {
    val expected = Color(r, g, b)
    val actual = color * 2.0
    Assert.assertTrue(expected.compare(actual))
}

@Then("c1 * c2 = color\\({double}, {double}, {double})")
fun checkColorMultiplication(r:Double, g:Double, b:Double) {
    val expected = Color(r, g, b)
    val actual = color1 * color2
    Assert.assertTrue(expected.compare(actual))
}

@Given("n ← vector\\({double}, {double}, {double})")
fun givenSurfaceNormalVector(x:Double, y:Double, z:Double) {
    surfaceNormal = Vector(x, y, z)
}

@When("r ← reflect\\(v, n)")
fun buildReflectionFromVectorAndNormal() {
    reflection = reflect(vector, surfaceNormal)
}

@Then("r = vector\\({int}, {int}, {int})")
fun testReflectionVector(x:Double, y:Double, z:Double) {
    val expected = Vector(x, y, z)
    assertTrue(expected.compare(reflection))
}