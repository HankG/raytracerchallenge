package org.hankg.raytracerchallenge.Intersections

import io.cucumber.java.en.Given
import io.cucumber.java.en.Then
import io.cucumber.java.en.When
import org.hankg.raytracerchallenge.MathFunctions.rotateZ
import org.hankg.raytracerchallenge.MathFunctions.scaling
import org.hankg.raytracerchallenge.MathFunctions.translate
import org.hankg.raytracerchallenge.Rays.direction
import org.hankg.raytracerchallenge.Rays.origin
import org.hankg.raytracerchallenge.Rays.ray
import org.hankg.raytracerchallenge.compare
import org.hankg.raytracerchallenge.model.*
import org.hankg.raytracerchallenge.tuples.point
import org.junit.Assert

var sphere = Sphere()
var intersection = Intersection()
var intersection1 = Intersection()
var intersection2 = Intersection()
var intersection3 = Intersection()
var intersection4 = Intersection()
var intersections = Intersections()
var normal = Vector.ZERO
var transform = Matrix.ZERO

@Given("s ← sphere")
fun givenSphere() {
    sphere = Sphere()
}

@When("i ← intersection\\({double}, s)")
fun intersectionAtTimeToSphere(t:Double) {
    intersection = Intersection(t, sphere)
}

@Then("i.t = {double}")
fun checkIntersectionTime(expectedT:Double) {
    Assert.assertTrue(compare(expectedT, intersection.t))
}

@Then("i.object = s")
fun compareIntersectionObjectEqualsSphere() {
    Assert.assertEquals(sphere, intersection.item)
}

@Given("i1 ← intersection\\({double}, s)")
fun givenTimeAndSphereGenIntersection1(t:Double) {
    intersection1 = Intersection(t, sphere)
}

@Given("i2 ← intersection\\({double}, s)")
fun givenTimeAndSphereGenIntersection2(t:Double) {
    intersection2 = Intersection(t, sphere)
}

@Given("i3 ← intersection\\({double}, s)")
fun givenTimeAndSphereGenIntersection3(t:Double) {
    intersection3 = Intersection(t, sphere)
}

@Given("i4 ← intersection\\({double}, s)")
fun givenTimeAndSphereGenIntersection4(t:Double) {
    intersection4 = Intersection(t, sphere)
}

@When("xs ← intersect\\(s, r)")
fun buildIntersectionsForSphereAndRay() {
    intersections = Intersections()
    intersections.addAll(Intersection.intersections(ray, sphere))
}

@When("xs ← intersections\\(i1, i2)")
fun createIntersectionsFromI1I2() {
    intersections = Intersections()
    intersections.add(intersection1)
    intersections.add(intersection2)
}

@When("xs ← intersections\\(i1, i2, i3, i4)")
fun createIntersectionsFromI1through4() {
    intersections = Intersections()
    intersections.add(intersection1)
    intersections.add(intersection2)
    intersections.add(intersection3)
    intersections.add(intersection4)
}

@When("xs ← intersections\\(i2, i1)")
fun createIntersectionsFromI2I1() {
    intersections = Intersections()
    intersections.add(intersection1)
    intersections.add(intersection2)
}

@Then("xs.count = {int}")
fun checkIntersectionsCount(expected:Int) {
    Assert.assertEquals(expected, intersections.count)
}

@Then("xs[{int}].t = {double}")
fun checkIntersectionsTimeAtIndex(index:Int, t:Double) {
    Assert.assertTrue(compare(t, intersections[index].t))
}

@Then("xs[{int}] = {double}")
fun getTimeAtIndex(index:Int, t:Double) {
    Assert.assertTrue(compare(t, intersections[index].t))
}

@When("i ← hit\\(xs)")
fun setIntersectionToHit() {
    intersection = intersections.hit()
}

@Then("i = i1")
fun checkIntersectionIsI1() {
    Assert.assertEquals(intersection1, intersection)
}

@Then("i = i2")
fun checkIntersectionIsI2() {
    Assert.assertEquals(intersection2, intersection)
}

@Then("i = i4")
fun checkIntersectionIsI4() {
    Assert.assertEquals(intersection4, intersection)
}

@Then("i is nothing")
fun checkIntersectionIsNothing() {
    Assert.assertEquals(Intersections.NO_INTERSECTION, intersection)
}

@Then("xs[{int}].object = s")
fun checkObjectAtIndexIsSphere(index:Int) {
    Assert.assertEquals(sphere, intersections[index].item)
}

@Then("s.transform = identity_matrix")
fun checkDefaultSphereTransform() {
    Assert.assertEquals(Matrix.I4, sphere.transform)
}

@Given("t ← translation\\({int}, {int}, {int})")
fun givenTranslationTransform(x:Double, y:Double, z:Double) {
    transform = translate(x, y, z)
}

@When("set_transform\\(s, t)")
fun setSphereTransform() {
    sphere.transform = transform
}

@When("sphere.transform = scaling\\({int}, {int}, {int})")
fun setSphereTransformToScaling(x:Double, y:Double, z:Double) {
    sphere.transform = scaling(x, y, z)
}

@When("sphere.transform = translation\\({int}, {int}, {int})")
fun setSphereTransformToTranslate(x:Double, y:Double, z:Double) {
    sphere.transform = translate(x, y, z)
}

@Then("s.transform = t")
fun checkSphereTransform() {
    Assert.assertEquals(sphere.transform, transform)
}

@When("n ← normal_at\\(s, p)")
fun setSphereNormalAtPoint() {
    normal = sphere.normal(point)
}

@Then("n = normalize\\(n)")
fun confirmNormalVectorIsNormalized() {
    val expected = normal.normalize()
    Assert.assertTrue(expected.compare(normal))
}

@Then("n = vector\\({double}, {double}, {double})")
fun checkNormalVector(x:Double, y:Double, z:Double) {
    val expected = Vector(x, y, z)
    Assert.assertTrue(expected.compare(normal, 1e-5))
}

@Given("t ← scaling\\({double}, {double}, {double}) * rotation_z\\({double})")
fun createScalingAndRotationZTransform(scaleX:Double, scaleY:Double, scaleZ:Double, rotateX:Double) {
    transform = scaling(scaleX, scaleY, scaleZ) * rotateZ(rotateX)
}
