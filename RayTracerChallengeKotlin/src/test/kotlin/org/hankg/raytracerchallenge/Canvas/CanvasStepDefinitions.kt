package org.hankg.raytracerchallenge.Canvas

import io.cucumber.java.en.Given
import io.cucumber.java.en.Then
import io.cucumber.java.en.When
import org.hankg.raytracerchallenge.io.PpmCanvasWriter
import org.hankg.raytracerchallenge.model.Canvas
import org.hankg.raytracerchallenge.model.Color
import org.hankg.raytracerchallenge.tuples.color1
import org.hankg.raytracerchallenge.tuples.color2
import org.hankg.raytracerchallenge.tuples.color3
import org.junit.Assert


val epsilon = 1e-12

var canvas: Canvas = Canvas(0, 0)
var ppm: List<String> = listOf()
var red: Color = Color.RED

// *********************************************************
// canvas checks

@Given("c ← canvas\\({int}, {int})")
fun givenCanvas(width:Int, height:Int) {
    println(color1)
    canvas = Canvas(width, height)
}

@Then("c.width = {int}")
fun checkCanvasWidth(width:Int) {
    Assert.assertEquals(width, canvas.width)
}

@Then("c.height = {int}")
fun checkCanvasHeight(height:Int) {
    Assert.assertEquals(height, canvas.height)
}

@Then("every pixel of c is color\\({double}, {double}, {double})")
fun checkAllPixelsOfColor(r:Double, g:Double, b:Double) {
    val expected = Color(r, g, b)
    for (x in 0 until canvas.width) {
        for (y in 0 until canvas.height) {
            Assert.assertTrue(expected.compare(canvas[x, y]))
        }
    }
}

@Given("red ← color\\({double}, {double}, {double})")
fun givenColorRed(r:Double, g:Double, b:Double) {
    red = Color(r, g, b)
}

@When("write_pixel\\(c, {int}, {int}, red)")
fun writeRedPixel(x:Int, y:Int) {
    canvas[x, y] = red
}

@Then("pixel_at\\(c, {int}, {int}) = red")
fun checkPixelIsRed(x:Int, y:Int) {
    Assert.assertTrue(red.compare(canvas[x, y]))
}

@When("ppm ← canvas_to_ppm\\(c)")
fun getPpm() {
    ppm = PpmCanvasWriter().generate(canvas)
}

@Then("lines {int}-{int} of ppm are")
fun comparePpmLines(start:Int, stop:Int, expected:String) {
    val actual = ppm.slice((start-1) until stop)
            .joinToString(separator = "\n")
    Assert.assertEquals(expected, actual)
}

@Then("ppm ends with a newline character")
fun ppmEndsWithNewLine() {
    Assert.assertEquals("\n", ppm.last())
}

@When("write_pixel\\(c, {int}, {int}, c1)")
fun writePixelUsingColor1(x:Int, y:Int) {
    canvas[x, y] = color1
}

@When("write_pixel\\(c, {int}, {int}, c2)")
fun writePixelUsingColor2(x:Int, y:Int) {
    canvas[x, y] = color2
}

@When("write_pixel\\(c, {int}, {int}, c3)")
fun writePixelUsingColor3(x:Int, y:Int) {
    canvas[x, y] = color3
}

@When("every pixel of c is set to color\\({double}, {double}, {double})")
fun writeColorToWholeCanvas(r:Double, g:Double, b:Double) {
    canvas.fillCanvas(Color(r, g, b))
}