package org.hankg.raytracerchallenge.Rays

import io.cucumber.java.en.Given
import io.cucumber.java.en.Then
import io.cucumber.java.en.When
import org.hankg.raytracerchallenge.MathFunctions.scaling
import org.hankg.raytracerchallenge.MathFunctions.translate
import org.hankg.raytracerchallenge.model.*
import org.junit.Assert

var origin = Point.ZERO
var direction = Vector.ZERO
var ray = Ray()
var ray2 = Ray()
var matrix = Matrix.ZERO

@Given("origin ← point\\({double}, {double}, {double})")
fun givenOrigin(x:Double, y:Double, z:Double) {
    origin = Point(x, y, z)
}

@Given("direction ← vector\\({int}, {int}, {int})")
fun givenDirection(x:Double, y:Double, z:Double) {
    direction = Vector(x, y, z)
}

@When("r ← ray\\(origin, direction)")
fun buildRay() {
    ray = Ray(origin, direction)
}

@Then("r.origin = origin")
fun testRayOrigin() {
    Assert.assertTrue(ray.origin.compare(origin))
}

@Then("r.direction = direction")
fun testRayDirection() {
    Assert.assertTrue(ray.direction.compare(direction))
}

@Then("position\\(r, {double}) = point\\({double}, {double}, {double})")
fun checkPositionAtTime(t:Double, x:Double, y:Double, z:Double) {
    val expected = Point(x, y, z)
    val actual = ray.position(t)
    Assert.assertTrue(expected.compare(actual))
}

@When("m ← scaling\\({double}, {double}, {double})")
fun transformIsScalingMatrix(x:Double, y:Double, z:Double) {
    matrix = scaling(x, y, z)
}

@When("m ← translation\\({int}, {int}, {int})")
fun tranformIsTranslation(x:Double, y:Double, z:Double) {
    matrix = translate(x, y, z)
}

@When("r2 ← transform\\(r, m)")
fun ray2FromTransform() {
    ray2 = ray.transform(matrix)
}

@Then("r2.origin = point\\({int}, {int}, {int})")
fun testRay2Origin(x:Double, y:Double, z:Double) {
    val expected = Point(x, y, z)
    Assert.assertTrue(ray2.origin.compare(expected))
}

@Then("r2.direction = vector\\({int}, {int}, {int})")
fun testRay2Direction(x:Double, y:Double, z:Double) {
    val expected = Vector(x, y, z)
    Assert.assertTrue(ray2.direction.compare(expected))
}
