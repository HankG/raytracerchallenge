package org.hankg.raytracerchallenge.Matrices

import io.cucumber.datatable.DataTable
import io.cucumber.java.en.Given
import io.cucumber.java.en.Then
import org.hankg.raytracerchallenge.MathFunctions.identity
import org.hankg.raytracerchallenge.model.Matrix
import org.hankg.raytracerchallenge.model.Tuple
import org.junit.Assert

val epsilon = 1e-12
var matrix = Matrix.ZERO
var matrixA = Matrix.ZERO
var matrixB = Matrix.ZERO
var matrixC = Matrix.ZERO
var tuple = Tuple.ZERO

fun DataTable.toMatrix(): Matrix{
    // Write code here that turns the phrase above into concrete actions
    // For automatic transformation, change DataTable to one of
    // E, List<E>, List<List<E>>, List<Map<K,V>>, Map<K,V> or
    // Map<K, List<V>>. E,K,V must be a String, Integer, Float,
    // Double, Byte, Short, Long, BigInteger or BigDecimal.
    //
    // For other transformations you can register a DataTableType.
    val newMatrix = this.asLists<Double>(Double::class.java)
    val matrix = Matrix(newMatrix[0].size, newMatrix.size)
    for (x in 0 until matrix.width) {
        for (y in 0 until matrix.height) {
            matrix[y, x] = newMatrix[y][x]
        }
    }

    return matrix
}

@Given("the following 2x2 matrix M:")
fun givenMatrix2x2(dataTable: DataTable) {
    matrix = dataTable.toMatrix()
}

@Given("the following 3x3 matrix M:")
fun givenMatrix3x3(dataTable: DataTable) {
    matrix = dataTable.toMatrix()
}

@Given("the following 4x4 matrix M:")
fun givenMatrix4x4(dataTable: DataTable) {
    matrix = dataTable.toMatrix()
}

@Then("M[{int}, {int}] = {double}")
fun thenMatrixValueEquals(x:Int, y:Int, expected:Double) {
    Assert.assertEquals(expected, matrix[x, y], epsilon)
}

@Given("the following matrix A:")
fun givenMatrixA(dataTable: DataTable) {
    matrixA = dataTable.toMatrix()
}

@Given("the following matrix B:")
fun givenMatrixB(dataTable: DataTable) {
    matrixB = dataTable.toMatrix()
}

@Then("A = B")
fun compareMatrixAandBEquality() {
    Assert.assertTrue(matrixA.compare(matrixB))
}

@Then("A != B")
fun compareMatrixAandBInequality() {
    Assert.assertFalse(matrixA.compare(matrixB))
}

@Then("A * B is the following 4x4 matrix:")
fun thenAandBEqualsThisResult(dataTable: DataTable) {
    val expected = dataTable.toMatrix()
    val actual = matrixA * matrixB
    Assert.assertTrue(expected.compare(actual))
}


@Given("b ← tuple\\({double}, {double}, {double}, {double})")
fun givenTuple(x:Double, y:Double, z:Double, w:Double) {
    tuple = Tuple(x, y, z, w)
}

@Then("A * b = tuple\\({double}, {double}, {double}, {double})")
fun thenAandbTupleEquals(x:Double, y:Double, z:Double, w:Double) {
    val expected = Tuple(x, y, z, w)
    val actual = matrixA * tuple
    Assert.assertTrue(expected.compare(actual))
}


@Then("A * identity_matrix = A")
fun checkATimesIdentityEquals() {
    val actual = matrixA * identity()
    Assert.assertTrue(matrixA.compare(actual))
}

@Then("identity_matrix * a = a")
fun checkIdentityTimesTupleEqualsTuple() {
    val actual = identity() * tuple
    Assert.assertTrue(tuple.compare(actual))
}

@Then("transpose\\(A) is the following matrix:")
fun checkATransposeEquals(dataTable: DataTable) {
    val expected = dataTable.toMatrix()
    val actual = matrixA.transpose()
    Assert.assertTrue(expected.compare(actual))
}

@Given("A ← transpose\\(identity_matrix)")
fun setMatrixToIdentityTranspose() {
    matrixA = identity().transpose()
}

@Then("A = identity_matrix")
fun compareMatrixAtoIdentity() {
    Assert.assertTrue(matrixA.compare(identity()))
}

@Given("the following 2x2 matrix A:")
fun given2x2MatrixA(dataTable: DataTable) {
    matrixA = dataTable.toMatrix()
}

@Given("the following 3x3 matrix A:")
fun given3x3MatrixA(dataTable: DataTable) {
    matrixA = dataTable.toMatrix()
}

@Given("the following 4x4 matrix A:")
fun given4x4MatrixA(dataTable: DataTable) {
    matrixA = dataTable.toMatrix()
}

@Given("the following 4x4 matrix B:")
fun given4x4MatrixB(dataTable: DataTable) {
    matrixB = dataTable.toMatrix()
}

@Then("determinant\\(A) = {double}")
fun matrixADeterminant(expected:Double) {
    Assert.assertEquals(expected, matrixA.determinant(), epsilon)
}

@Then("determinant\\(B) = {double}")
fun matrixBDeterminant(expected:Double) {
    Assert.assertEquals(expected, matrixB.determinant(), epsilon)
}

@Then("submatrix\\(A, {int}, {int}) is the following 2x2 matrix:")
fun compareExpectedSub2x2Matrix(rowToExclude:Int, columnToExclude:Int, dataTable: DataTable) {
    val expected = dataTable.toMatrix()
    val actual = matrixA.subMatrix(rowToExclude, columnToExclude)
    Assert.assertTrue(expected.compare(actual))
}

@Then("submatrix\\(A, {int}, {int}) is the following 3x3 matrix:")
fun compareExpectedSub3x3Matrix(rowToExclude:Int, columnToExclude:Int, dataTable: DataTable) {
    val expected = dataTable.toMatrix()
    val actual = matrixA.subMatrix(rowToExclude, columnToExclude)
    Assert.assertTrue(expected.compare(actual))
}

@Given("B ← submatrix\\(A, {int}, {int})")
fun givenSubmatrixBofA(row:Int, column:Int) {
    matrixB = matrixA.subMatrix(row, column)
}

@Then("minor\\(A, {int}, {int}) = {double}")
fun compareMinorA(row:Int, column:Int, expected:Double) {
    val actual = matrixA.minor(row, column)
    Assert.assertEquals(expected, actual, epsilon)
}

@Then("cofactor\\(A, {int}, {int}) = {double}")
fun compareCofactorA(row:Int, column:Int, expected:Double) {
    val actual = matrixA.cofactor(row, column)
    Assert.assertEquals(expected, actual, epsilon)
}

@Then("A is invertible")
fun checkMatrixAInvertible() {
    Assert.assertTrue(matrixA.isInvertable())
}

@Then("A is not invertible")
fun checkMatrixANotInvertible() {
    Assert.assertFalse(matrixA.isInvertable())
}

@Given("B ← inverse\\(A)")
fun invertMatrixAandStoreInB() {
    matrixB = matrixA.invert()
}

@Then("B[{int}, {int}] = {double}\\/{double}")
fun checkBValueAtIndex(row:Int, column:Int, numerator:Double, denominator:Double) {
    val expected = numerator / denominator
    val actual = matrixB[row, column]
    Assert.assertEquals(expected, actual, epsilon)

}

@Then("B is the following 4x4 matrix:")
fun thenBEqualsThisResult(dataTable: DataTable) {
    val expected = dataTable.toMatrix()
    val actual = matrixB
    val tolerance = 1e-5
    Assert.assertTrue(expected.compare(actual, tolerance))
}

@Then("inverse\\(A) is the following 4x4 matrix:")
fun testAInverseEquals(dataTable: DataTable) {
    val expected = dataTable.toMatrix()
    val actual = matrixA.invert()
    val tolerance = 1e-5
    Assert.assertTrue(expected.compare(actual, tolerance))
}

@Given("C ← A * B")
fun givenMatrixCFromABProduct() {
    matrixC = matrixA * matrixB
}

@Then("C * inverse\\(B) = A")
fun checkCTimesInverseBEqualsA() {
    val expected = matrixA
    val actual = matrixC * matrixB.invert()
    Assert.assertTrue(expected.compare(actual))
}