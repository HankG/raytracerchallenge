# RayTracerChallenge

The repository for my go at doing the [Ray Tracer Challenge](http://www.raytracerchallenge.com/) project

I'm keeping a development log within the project a `devlog.txt` document in each respective subfolder.
This is based off the logs that John Carmack kept back in the day, but with some tweaks. 
The legend for the log is:

Each day begins with the header:
```
==== DD MMM YYYY ==================
```

The legend for each line leader is:
```
'' (no leader): A note
* : An item to do later
# : Item completed same day
+ : Item completed but on another day
- : Item going to be ignored
T : Item transfered from one log to another
= : Sub-bullets of notes/tasks that aren't tasks themselves
```

