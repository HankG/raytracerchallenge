import 'package:raytracerchallenge/model/color.dart';
import 'package:raytracerchallenge/model/tuple.dart';

import '../steps/steps_utils.dart';

List<num> numbersFromString(String string) =>
    string.split(',').map((s1) => interpretNumber(s1)).toList();

Color colorFromString(String string) {
  final elements = numbersFromString(string);
  return color(elements[0], elements[1], elements[2]);
}

Tuple pointFromString(String string) {
  final elements = numbersFromString(string);
  return point(elements[0], elements[1], elements[2]);
}

Tuple vectorFromString(String string) {
  final elements = numbersFromString(string);
  return vector(elements[0], elements[1], elements[2]);
}

Tuple tupleFromString(String string) {
  final elements = numbersFromString(string);
  return Tuple(elements[0], elements[1], elements[2], elements[3]);
}
