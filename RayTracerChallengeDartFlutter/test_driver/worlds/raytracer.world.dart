import 'package:gherkin/gherkin.dart';
import 'package:raytracerchallenge/model/canvas.dart';
import 'package:raytracerchallenge/model/color.dart';
import 'package:raytracerchallenge/model/intersection.dart';
import 'package:raytracerchallenge/model/light.dart';
import 'package:raytracerchallenge/model/matrix.dart';
import 'package:raytracerchallenge/model/ray.dart';
import 'package:raytracerchallenge/model/sphere.dart';
import 'package:raytracerchallenge/model/surface_material.dart';
import 'package:raytracerchallenge/model/tuple.dart';

class RayTracerWorld extends World {
  Tuple a = Tuple.ZERO;
  Tuple a1 = Tuple.ZERO;
  Tuple a2 = Tuple.ZERO;
  Tuple b = Tuple.ZERO;
  Tuple p = point(0, 0, 0);
  Tuple p1 = point(0, 0, 0);
  Tuple p2 = point(0, 0, 0);
  Tuple p3 = point(0, 0, 0);
  Tuple p4 = point(0, 0, 0);
  Tuple v = vector(0, 0, 0);
  Tuple v1 = vector(0, 0, 0);
  Tuple v2 = vector(0, 0, 0);
  Matrix matrixM = Matrix(1, 1);
  Matrix matrixA = Matrix(1, 1);
  Matrix matrixB = Matrix(1, 1);
  Matrix matrixC = Matrix(1, 1);
  Tuple norm = vector(1, 0, 0);
  Tuple zero = vector(0, 0, 0);
  Color c = BLACK;
  Color c1 = BLACK;
  Color c2 = BLACK;
  Color c3 = BLACK;
  Color red = BLACK;
  Canvas canvas = Canvas(0, 0);
  String ppmData = '';
  Matrix transform = Matrix.identity();
  Matrix transformInverse = Matrix.identity();
  Matrix halfQuarter = Matrix.identity();
  Matrix fullQuarter = Matrix.identity();
  Tuple origin = point(0, 0, 0);
  Tuple direction = vector(0, 0, 0);
  Ray r = ray(point(0, 0, 0), vector(0, 0, 0));
  Ray r2 = ray(point(0, 0, 0), vector(0, 0, 0));
  Sphere s = sphere();
  Intersection i = Intersection(0, sphere());
  Intersection i1 = Intersection(0, sphere());
  Intersection i2 = Intersection(0, sphere());
  Intersection i3 = Intersection(0, sphere());
  Intersection i4 = Intersection(0, sphere());
  Intersections intersections = Intersections.none();
  Tuple normal = Tuple.ZERO;
  Tuple reflected = Tuple.ZERO;
  Light light = Light(const Color(1, 1, 1), point(100, 200, 300));
  SurfaceMaterial surfaceMaterial = material();
  Tuple eyeV = Tuple.ZERO;
  Color lightCalcResult = BLACK;
}
