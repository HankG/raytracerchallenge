import 'package:gherkin/gherkin.dart';

import 'steps/canvas_steps.dart';
import 'steps/color_steps.dart';
import 'steps/intersections_steps.dart';
import 'steps/lights_steps.dart';
import 'steps/material_steps.dart';
import 'steps/matrices_steps.dart';
import 'steps/rays_steps.dart';
import 'steps/sphere_steps.dart';
import 'steps/transforms_steps.dart';
import 'steps/tuple_steps.dart';
import 'worlds/raytracer.world.dart';

Future<void> main() async {
  final steps = [
    ...materialSteps,
    ...tupleSteps,
    ...colorSteps,
    ...canvasSteps,
    ...matricesSteps,
    ...transformsSteps,
    ...raySteps,
    ...sphereSteps,
    ...intersectionsSteps,
    ...lightsSteps,
  ];

  final config = TestConfiguration()
    ..features = [RegExp('features/*.*.feature')]
    ..reporters = [
      StdoutReporter(),
      ProgressReporter(),
      TestRunSummaryReporter(),
      JsonReporter(),
    ]
    ..stepDefinitions = steps
    ..stopAfterTestFailed = true
    ..createWorld = (config) => Future.value(RayTracerWorld());

  return GherkinRunner().execute(config);
}
