import 'dart:math';

List<String> strippedElements(String original) {
  return original
      .split(',')
      .map((e) => !e.contains('(') ? e : e.substring(e.indexOf('(') + 1))
      .map((e) => !e.contains(')') ? e : e.substring(0, e.lastIndexOf(')')))
      .map((e) => e.trim())
      .toList();
}

num expressionToNumber(String value) {
  final elements = value.split('/');
  if (elements.length == 1) {
    return interpretNumber(elements.first);
  }

  if (elements.length == 2) {
    final numerator = interpretNumber(elements.first.trim());
    final denominator = interpretNumber(elements.last.trim());
    return numerator / denominator;
  }

  throw Exception('Too many elements parse: $value');
}

num interpretNumber(String number) {
  if (number.contains('/')) {
    final components = number.split('/');
    final numerator = interpretNumber(components.first.trim());
    final denominator = interpretNumber(components.last.trim());
    return numerator / denominator;
  }

  if (number == 'π') {
    return pi;
  }

  if (number.contains('√')) {
    int start = 1;
    num multiplier = 1;
    if (number.startsWith('-')) {
      multiplier = -1;
      start = 2;
    }
    return multiplier * sqrt(num.parse(number.substring(start)));
  }

  return num.parse(number);
}
