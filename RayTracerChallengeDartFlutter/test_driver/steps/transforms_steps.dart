import 'dart:math';

import 'package:gherkin/gherkin.dart';
import 'package:raytracerchallenge/model/tuple.dart';
import 'package:raytracerchallenge/utils/transforms.dart';

import '../worlds/raytracer.world.dart';
import 'steps_utils.dart';

final transformsSteps = [
  givenTranslation(),
  givenScale(),
  givenShear(),
  givenTransformInverse(),
  givenHalfQuarterXRotation(),
  givenHalfQuarterYRotation(),
  givenHalfQuarterZRotation(),
  givenFullQuarterXRotation(),
  givenFullQuarterYRotation(),
  givenFullQuarterZRotation(),
  givenInverseOfHalfQuarter(),
  givenARotatesXFullQuarter(),
  givenBScales(),
  givenCTranslates(),
  whenP2EqualsATimesP(),
  whenP3EqualsBTimesP2(),
  whenP4EqualsCTimes3(),
  whenTEqualsCBA(),
  thenTransformTimesPEqualsPoint(),
  thenTransformInverseTimesPEqualsPoint(),
  thenTransformTimesVectorEquals(),
  thenTransformInverseTimesVectorEquals(),
  thenTransformDoesntChangeVector(),
  thenHalfQuarterTimesPEquals(),
  thenFullQuarterTimesPEquals(),
  thenP2Equals(),
  thenP3Equals(),
  thenP4Equals(),
  thenTransformTimesPEquals(),
];

// Givens

StepDefinitionGeneric givenTranslation() =>
    given1<String, RayTracerWorld>(r'transform ← translation\(([^)]+)\)',
        (elementsString, context) async {
      final elements =
          elementsString.split(',').map((e) => num.parse(e)).toList();
      context.world.transform = translate(
        elements[0],
        elements[1],
        elements[2],
      );
    });

StepDefinitionGeneric givenScale() =>
    given1<String, RayTracerWorld>(r'transform ← scaling\(([^)]+)\)',
        (elementsString, context) async {
      final elements =
          elementsString.split(',').map((e) => num.parse(e)).toList();
      context.world.transform = scale(
        elements[0],
        elements[1],
        elements[2],
      );
    });

StepDefinitionGeneric givenShear() =>
    given1<String, RayTracerWorld>(r'transform ← shearing\(([^)]+)\)',
        (elementsString, context) async {
      final elements =
          elementsString.split(',').map((e) => num.parse(e)).toList();
      context.world.transform = shear(
        elements[0],
        elements[1],
        elements[2],
        elements[3],
        elements[4],
        elements[5],
      );
    });

StepDefinitionGeneric givenTransformInverse() =>
    given<RayTracerWorld>(r'inv ← inverse\(transform\)', (context) async {
      context.world.transformInverse = context.world.transform.invert();
    });

StepDefinitionGeneric givenHalfQuarterXRotation() =>
    given<RayTracerWorld>(r'half_quarter ← rotation_x\(π / 4\)',
        (context) async {
      context.world.halfQuarter = rotateX(pi / 4);
    });

StepDefinitionGeneric givenHalfQuarterYRotation() =>
    given<RayTracerWorld>(r'half_quarter ← rotation_y\(π / 4\)',
        (context) async {
      context.world.halfQuarter = rotateY(pi / 4);
    });

StepDefinitionGeneric givenHalfQuarterZRotation() =>
    given<RayTracerWorld>(r'half_quarter ← rotation_z\(π / 4\)',
        (context) async {
      context.world.halfQuarter = rotateZ(pi / 4);
    });

StepDefinitionGeneric givenFullQuarterXRotation() =>
    given<RayTracerWorld>(r'full_quarter ← rotation_x\(π / 2\)',
        (context) async {
      context.world.fullQuarter = rotateX(pi / 2);
    });

StepDefinitionGeneric givenFullQuarterYRotation() =>
    given<RayTracerWorld>(r'full_quarter ← rotation_y\(π / 2\)',
        (context) async {
      context.world.fullQuarter = rotateY(pi / 2);
    });

StepDefinitionGeneric givenFullQuarterZRotation() =>
    given<RayTracerWorld>(r'full_quarter ← rotation_z\(π / 2\)',
        (context) async {
      context.world.fullQuarter = rotateZ(pi / 2);
    });

StepDefinitionGeneric givenInverseOfHalfQuarter() =>
    given<RayTracerWorld>(r'inv ← inverse\(half_quarter\)', (context) async {
      context.world.transformInverse = context.world.halfQuarter.invert();
    });

StepDefinitionGeneric givenARotatesXFullQuarter() =>
    given<RayTracerWorld>(r'A ← rotation_x\(π / 2\)', (context) async {
      context.world.matrixA = rotateX(pi / 2);
    });

StepDefinitionGeneric givenBScales() =>
    given1<String, RayTracerWorld>(r'B ← scaling\(([^)]+)\)',
        (elementsString, context) async {
      final elements =
          elementsString.split(',').map((e) => expressionToNumber(e)).toList();
      context.world.matrixB = scale(elements[0], elements[1], elements[2]);
    });

StepDefinitionGeneric givenCTranslates() =>
    given1<String, RayTracerWorld>(r'C ← translation\(([^)]+)\)',
        (elementsString, context) async {
      final elements =
          elementsString.split(',').map((e) => expressionToNumber(e)).toList();
      context.world.matrixC = translate(elements[0], elements[1], elements[2]);
    });

// Whens
StepDefinitionGeneric whenP2EqualsATimesP() =>
    given<RayTracerWorld>(r'p2 ← A \* p', (context) async {
      context.world.p2 = context.world.matrixA.times(context.world.p);
    });

StepDefinitionGeneric whenP3EqualsBTimesP2() =>
    given<RayTracerWorld>(r'p3 ← B \* p2', (context) async {
      context.world.p3 = context.world.matrixB.times(context.world.p2);
    });

StepDefinitionGeneric whenP4EqualsCTimes3() =>
    given<RayTracerWorld>(r'p4 ← C \* p3', (context) async {
      context.world.p4 = context.world.matrixC.times(context.world.p3);
    });

//'When T ← C * B * A'
StepDefinitionGeneric whenTEqualsCBA() =>
    given<RayTracerWorld>(r'T ← C \* B \* A', (context) async {
      context.world.transform =
          context.world.matrixC * context.world.matrixB * context.world.matrixA;
    });
// Thens

StepDefinitionGeneric thenTransformTimesPEqualsPoint() =>
    given1<String, RayTracerWorld>(r'transform \* p = point\(([^)]+)\)',
        (elementsString, context) async {
      final elements =
          elementsString.split(',').map((e) => num.parse(e)).toList();
      final expected = point(elements[0], elements[1], elements[2]);
      final actual = context.world.transform.times(context.world.p);
      context.expectMatch(actual, expected);
    });

StepDefinitionGeneric thenTransformInverseTimesPEqualsPoint() =>
    given1<String, RayTracerWorld>(r'inv \* p = point\(([^)]+)\)',
        (elementsString, context) async {
      final elements =
          elementsString.split(',').map((e) => expressionToNumber(e)).toList();
      final expected = point(elements[0], elements[1], elements[2]);
      final actual = context.world.transformInverse.times(context.world.p);
      context.expectMatch(actual.equals(expected, tolerance: 1e-12), true);
    });

StepDefinitionGeneric thenTransformTimesVectorEquals() =>
    given1<String, RayTracerWorld>(r'transform \* v = vector\(([^)]+)\)',
        (elementsString, context) async {
      final elements =
          elementsString.split(',').map((e) => num.parse(e)).toList();
      final expected = vector(elements[0], elements[1], elements[2]);
      final actual = context.world.transform.times(context.world.v);
      context.expectMatch(actual, expected);
    });

StepDefinitionGeneric thenTransformInverseTimesVectorEquals() =>
    given1<String, RayTracerWorld>(r'inv \* v = vector\(([^)]+)\)',
        (elementsString, context) async {
      final elements =
          elementsString.split(',').map((e) => num.parse(e)).toList();
      final expected = vector(elements[0], elements[1], elements[2]);
      final actual = context.world.transformInverse.times(context.world.v);
      context.expectMatch(actual, expected);
    });

StepDefinitionGeneric thenTransformDoesntChangeVector() =>
    given<RayTracerWorld>(r'transform \* v = v', (context) async {
      context.expectMatch(
        context.world.transform.times(context.world.v),
        context.world.v,
      );
    });

StepDefinitionGeneric thenHalfQuarterTimesPEquals() =>
    given1<String, RayTracerWorld>(r'half_quarter \* p = point\(([^)]+)\)',
        (elementsString, context) async {
      final elements =
          elementsString.split(',').map((e) => expressionToNumber(e)).toList();
      final expected = point(elements[0], elements[1], elements[2]);
      final actual = context.world.halfQuarter.times(context.world.p);
      context.expectMatch(actual.equals(expected, tolerance: 1e-12), true);
    });

StepDefinitionGeneric thenFullQuarterTimesPEquals() =>
    given1<String, RayTracerWorld>(r'full_quarter \* p = point\(([^)]+)\)',
        (elementsString, context) async {
      final elements =
          elementsString.split(',').map((e) => expressionToNumber(e)).toList();
      final expected = point(elements[0], elements[1], elements[2]);
      final actual = context.world.fullQuarter.times(context.world.p);
      context.expectMatch(actual.equals(expected, tolerance: 1e-12), true);
    });

StepDefinitionGeneric thenP2Equals() =>
    given1<String, RayTracerWorld>(r'p2 = point\(([^)]+)\)',
        (elementsString, context) async {
      final elements =
          elementsString.split(',').map((e) => expressionToNumber(e)).toList();
      final expected = point(elements[0], elements[1], elements[2]);
      context.expectMatch(
          context.world.p2.equals(expected, tolerance: 1e-12), true);
    });

StepDefinitionGeneric thenP3Equals() =>
    given1<String, RayTracerWorld>(r'p3 = point\(([^)]+)\)',
        (elementsString, context) async {
      final elements =
          elementsString.split(',').map((e) => expressionToNumber(e)).toList();
      final expected = point(elements[0], elements[1], elements[2]);
      context.expectMatch(
          context.world.p3.equals(expected, tolerance: 1e-12), true);
    });

StepDefinitionGeneric thenTransformTimesPEquals() =>
    given1<String, RayTracerWorld>(r'T \* p = point\(([^)]+)\)',
        (elementsString, context) async {
      final elements =
          elementsString.split(',').map((e) => expressionToNumber(e)).toList();
      final expected = point(elements[0], elements[1], elements[2]);
      final actual = context.world.transform.times(context.world.p);
      context.expectMatch(actual.equals(expected, tolerance: 1e-12), true);
    });

StepDefinitionGeneric thenP4Equals() =>
    given1<String, RayTracerWorld>(r'p4 = point\(([^)]+)\)',
        (elementsString, context) async {
      final elements =
          elementsString.split(',').map((e) => expressionToNumber(e)).toList();
      final expected = point(elements[0], elements[1], elements[2]);
      context.expectMatch(
          context.world.p4.equals(expected, tolerance: 1e-12), true);
    });
