import 'package:gherkin/gherkin.dart';
import 'package:raytracerchallenge/model/constants.dart';
import 'package:raytracerchallenge/model/matrix.dart';
import 'package:raytracerchallenge/model/sphere.dart';
import 'package:raytracerchallenge/model/surface_material.dart';
import 'package:raytracerchallenge/model/tuple.dart';
import 'package:raytracerchallenge/utils/transforms.dart';

import '../worlds/raytracer.world.dart';
import 'steps_utils.dart';

final sphereSteps = [
  givenSphere(),
  givenTranslationTransform(),
  givenSphereTransform(),
  whenIntersectOfRayAndSphere(),
  whenSetSphereTransform(),
  whenSetSphereTransformToScaling(),
  whenSetSphereTransformToTranslation(),
  whenNormalAt(),
  whenSphereMaterial(),
  whenSettingSphereMaterial(),
  thenXsCountEquals(),
  thenXsTimeOfIndexEquals(),
  thenXsObjectEqualsSObject(),
  thenSphereTransformEqualsIdentity(),
  thenSphereTransformEqualsT(),
  thenNormalEquals(),
  thenNormalIsNormalized(),
  thenMaterialEqualsDefault(),
  thenSphereMaterialEquals(),
];

// Givens
StepDefinitionGeneric givenSphere() =>
    given<RayTracerWorld>(r's ← sphere\(\)', (context) async {
      context.world.s = sphere();
    });

StepDefinitionGeneric givenTranslationTransform() =>
    given1<String, RayTracerWorld>(r't ← translation\((.*)\)',
        (translationData, context) async {
      final elements =
          strippedElements(translationData).map((e) => num.parse(e)).toList();
      context.world.transform =
          translate(elements[0], elements[1], elements[2]);
    });

StepDefinitionGeneric givenSphereTransform() =>
    given<RayTracerWorld>(r'set_transform\(s, m\)', (context) async {
      context.world.s =
          context.world.s.updateTransform(context.world.transform);
    });

// Whens
StepDefinitionGeneric whenIntersectOfRayAndSphere() =>
    given<RayTracerWorld>(r'xs ← intersect\(s, r\)', (context) async {
      final intersection = context.world.s.intersect(context.world.r);
      context.world.intersections = intersection;
    });

StepDefinitionGeneric whenSetSphereTransform() =>
    given<RayTracerWorld>(r'set_transform\(s, t\)', (context) async {
      context.world.s =
          context.world.s.updateTransform(context.world.transform);
    });

StepDefinitionGeneric whenSetSphereTransformToScaling() =>
    given1<String, RayTracerWorld>(r'set_transform\(s, scaling\((.*)\)\)',
        (scalingData, context) async {
      final elements =
          strippedElements(scalingData).map((e) => num.parse(e)).toList();
      final transform = scale(elements[0], elements[1], elements[2]);
      context.world.s = context.world.s.updateTransform(transform);
    });

StepDefinitionGeneric whenSetSphereTransformToTranslation() =>
    given1<String, RayTracerWorld>(r'set_transform\(s, translation\((.*)\)\)',
        (scalingData, context) async {
      final elements =
          strippedElements(scalingData).map((e) => num.parse(e)).toList();
      final transform = translate(elements[0], elements[1], elements[2]);
      context.world.s = context.world.s.updateTransform(transform);
    });

StepDefinitionGeneric whenNormalAt() =>
    given1<String, RayTracerWorld>(r'n ← normal_at\(s, point\((.*)\)\)',
        (pointData, context) async {
      final elements =
          strippedElements(pointData).map((e) => interpretNumber(e)).toList();
      final p = point(elements[0], elements[1], elements[2]);
      context.world.normal = context.world.s.normal(p);
    });

StepDefinitionGeneric whenSphereMaterial() =>
    given<RayTracerWorld>(r'm ← s.material', (context) async {
      context.world.surfaceMaterial = context.world.s.surfaceMaterial;
    });

StepDefinitionGeneric whenSettingSphereMaterial() =>
    given<RayTracerWorld>(r's.material ← m', (context) async {
      context.world.s =
          context.world.s.updateMaterial(context.world.surfaceMaterial);
    });

// Thens
StepDefinitionGeneric thenXsCountEquals() =>
    given1<num, RayTracerWorld>('xs.count = {num}', (expected, context) async {
      context.expectMatch(context.world.intersections.count, expected);
    });

StepDefinitionGeneric thenXsTimeOfIndexEquals() =>
    given2<num, num, RayTracerWorld>(r'xs\[{num}\] = {num}',
        (index, expected, context) async {
      final actual = switch (index) {
        0 => context.world.intersections.xs.first.t,
        1 => context.world.intersections.xs.last.t,
        _ => throw Exception('Unknown index')
      };
      context.expectMatch(actual, expected);
    });

StepDefinitionGeneric thenXsObjectEqualsSObject() =>
    given1<num, RayTracerWorld>(r'xs\[{num}\].object = s',
        (index, context) async {
      final actual = switch (index) {
        0 => context.world.intersections.xs.first.object,
        1 => context.world.intersections.xs.last.object,
        _ => throw Exception('Unknown index')
      };
      context.expectMatch(actual, context.world.s);
    });

StepDefinitionGeneric thenSphereTransformEqualsIdentity() =>
    given<RayTracerWorld>('s.transform = identity_matrix', (context) async {
      context.expectMatch(context.world.s.transform, Matrix.identity());
    });

StepDefinitionGeneric thenSphereTransformEqualsT() =>
    given<RayTracerWorld>('s.transform = t', (context) async {
      context.expectMatch(
        context.world.s.transform,
        context.world.transform,
      );
    });

StepDefinitionGeneric thenNormalEquals() =>
    given1<String, RayTracerWorld>(r'n = vector\((.*)\)',
        (expectedData, context) async {
      final elements = strippedElements(expectedData)
          .map((e) => interpretNumber(e))
          .toList();
      final tolerance = expectedData.contains('/') ? epsilon : 1e-5;
      final expected = vector(elements[0], elements[1], elements[2]);
      context.expectMatch(
        context.world.normal.equals(
          expected,
          tolerance: tolerance,
        ),
        true,
        reason: 'Expected: $expected   Actual: ${context.world.normal}',
      );
    });

StepDefinitionGeneric thenNormalIsNormalized() =>
    given<RayTracerWorld>(r'n = normalize\(n\)', (context) async {
      context.expectMatch(
        context.world.normal.magnitude,
        1.0,
      );
    });

StepDefinitionGeneric thenMaterialEqualsDefault() =>
    given<RayTracerWorld>(r'm = material\(\)', (context) async {
      context.expectMatch(
        context.world.s.surfaceMaterial,
        material(),
      );
    });

StepDefinitionGeneric thenSphereMaterialEquals() =>
    given<RayTracerWorld>(r's.material = m', (context) async {
      context.expectMatch(
        context.world.s.surfaceMaterial,
        context.world.surfaceMaterial,
      );
    });
