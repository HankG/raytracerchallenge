import 'package:gherkin/gherkin.dart';
import 'package:raytracerchallenge/model/intersection.dart';

import '../worlds/raytracer.world.dart';
import 'steps_utils.dart';

final intersectionsSteps = <StepDefinitionGeneric<World>>[
  givenIntersection(),
  givenIntersectionVarName(),
  givenIntersectionI1I2(),
  givenIntersectionI2I1(),
  givenIntersectionI1I2I3I4(),
  whenIEqualsHitXS(),
  thenTimeOfIntersectionEquals(),
  thenObjectEqualsS(),
  thenXsTimeOfIndexEquals2(),
  thenIEquals(),
];

// Given
StepDefinitionGeneric givenIntersection() =>
    given1<String, RayTracerWorld>(r'i ← intersection\((.*)\)',
        (intersectionData, context) async {
      final elements = strippedElements(intersectionData);
      if (elements[1] != 's') {
        throw Exception('Only know how to check against the sphere object');
      }
      final t = num.parse(elements[0]);
      context.world.i = Intersection(t, context.world.s);
    });

StepDefinitionGeneric givenIntersectionVarName() =>
    given2<String, String, RayTracerWorld>(r' (.*) ← intersection\((.*)\)',
        (varName, intersectionData, context) async {
      final elements = strippedElements(intersectionData);
      if (elements[1] != 's') {
        throw Exception('Only know how to check against the sphere object');
      }
      final t = num.parse(elements[0]);
      final intersection = Intersection(t, context.world.s);
      switch (varName.trim()) {
        case 'i1':
          context.world.i1 = intersection;
          break;
        case 'i2':
          context.world.i2 = intersection;
          break;
        case 'i3':
          context.world.i3 = intersection;
          break;
        case 'i4':
          context.world.i4 = intersection;
          break;
        default:
          throw Exception('Unknown variable name: $varName');
      }
    });

StepDefinitionGeneric givenIntersectionI1I2() =>
    given<RayTracerWorld>(r'xs ← intersections\(i1, i2\)', (context) async {
      context.world.intersections = Intersections([
        context.world.i1,
        context.world.i2,
      ]);
    });

StepDefinitionGeneric givenIntersectionI2I1() =>
    given<RayTracerWorld>(r'xs ← intersections\(i2, i1\)', (context) async {
      context.world.intersections = Intersections([
        context.world.i2,
        context.world.i1,
      ]);
    });

StepDefinitionGeneric givenIntersectionI1I2I3I4() =>
    given<RayTracerWorld>(r'xs ← intersections\(i1, i2, i3, i4\)',
        (context) async {
      context.world.intersections = Intersections([
        context.world.i1,
        context.world.i2,
        context.world.i3,
        context.world.i4,
      ]);
    });

// Whens

StepDefinitionGeneric whenIEqualsHitXS() =>
    given<RayTracerWorld>(r'i ← hit\(xs\)', (context) async {
      context.world.i = context.world.intersections.hit();
    });

// Thens
StepDefinitionGeneric thenTimeOfIntersectionEquals() =>
    given1<num, RayTracerWorld>(r'i.t = {num}', (time, context) async {
      context.expectMatch(time, context.world.i.t);
    });

StepDefinitionGeneric thenObjectEqualsS() =>
    given<RayTracerWorld>(r'i.object = s', (context) async {
      context.expectMatch(context.world.i.object, context.world.s);
    });

StepDefinitionGeneric thenXsTimeOfIndexEquals2() =>
    given2<int, num, RayTracerWorld>(r'xs\[{int}\].t = {num}',
        (index, expected, context) async {
      final actual = context.world.intersections.xs[index.toInt()].t;
      context.expectMatch(actual, expected);
    });

StepDefinitionGeneric thenIEquals() =>
    given2<String, String, RayTracerWorld>(r'i (.*) (.*)',
        (comparisonText, expectedName, context) async {
      if (comparisonText.trim() != '=' && comparisonText.trim() != 'is') {
        throw Exception('Unknown comparison type: $comparisonText');
      }
      final expected = switch (expectedName.trim()) {
        'i1' => context.world.i1,
        'i2' => context.world.i2,
        'i3' => context.world.i3,
        'i4' => context.world.i4,
        'nothing' => NO_INTERSECTION,
        _ =>
          throw Exception('Unknown intersection variable name: $expectedName')
      };

      context.expectMatch(context.world.i, expected);
    });
