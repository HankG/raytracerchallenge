import 'package:gherkin/gherkin.dart';
import 'package:raytracerchallenge/model/canvas.dart';
import 'package:raytracerchallenge/writers/canvas_ppm_writer.dart';

import '../utils/string_utils.dart';
import '../worlds/raytracer.world.dart';

final canvasSteps = [
  GivenCanvas(),
  GivenRed(),
  WhenWritePixel(),
  whenWritePpm(),
  whenEveryPixelSetToColor(),
  thenCanvasWidth(),
  thenCanvasHeight(),
  ThenEveryPixelCheck(),
  ThenPixelIsRed(),
  thenPpmLines1to3Equal(),
  thenPpmLines4to6Equal(),
  thenPpmLines4to7Equal(),
  thenPpmEndsWithNewLine(),
];

//Givens

class GivenCanvas extends Given1<String> {
  final _exp = r"\(([^)]+)\)";

  @override
  RegExp get pattern => RegExp('c ← canvas$_exp');

  @override
  Future<void> executeStep(String input1) async {
    final w = world as RayTracerWorld;
    final dim = numbersFromString(input1);
    w.canvas = Canvas(dim[0].toInt(), dim[1].toInt());
  }
}

class GivenRed extends Given1<String> {
  final _exp = r"\(([^)]+)\)";

  @override
  RegExp get pattern => RegExp('red ← color$_exp');

  @override
  Future<void> executeStep(String input1) async {
    final w = world as RayTracerWorld;
    w.red = colorFromString(input1);
  }
}

// Whens
class WhenWritePixel extends Given1<String> {
  @override
  RegExp get pattern => RegExp(r"write_pixel\(([^)]+)\)");

  @override
  Future<void> executeStep(String input1) async {
    final w = world as RayTracerWorld;
    final elements = input1.split(',').map((e) => e.trim()).toList();
    final column = int.parse(elements[1]);
    final row = int.parse(elements[2]);
    final color = switch (elements[3]) {
      'red' => w.red,
      'c1' => w.c1,
      'c2' => w.c2,
      'c3' => w.c3,
      _ => throw Exception('Unknown color: ${elements[3]}'),
    };
    w.canvas.setPixel(column, row, color);
  }
}

StepDefinitionGeneric whenWritePpm() =>
    given<RayTracerWorld>(r"ppm ← canvas_to_ppm\(c\)", (context) async {
      context.world.ppmData = context.world.canvas.toPPMString();
    });

StepDefinitionGeneric whenEveryPixelSetToColor() =>
    given1<String, RayTracerWorld>(
        r"every pixel of c is set to color\(([^)]+)\)",
        (colorText, context) async {
      final color = colorFromString(colorText);
      context.world.canvas.fill(color);
    });

// Thens

StepDefinitionGeneric thenCanvasWidth() =>
    given1<num, RayTracerWorld>('c.width = {num}', (width, context) async {
      context.expectMatch(context.world.canvas.width, width);
    });

StepDefinitionGeneric thenCanvasHeight() =>
    given1<num, RayTracerWorld>('c.height = {num}', (width, context) async {
      context.expectMatch(context.world.canvas.height, width);
    });

class ThenEveryPixelCheck extends Given1<String> {
  final _exp = r"\(([^)]+)\)";

  @override
  RegExp get pattern => RegExp('every pixel of c is color$_exp');

  @override
  Future<void> executeStep(String input1) async {
    final canvas = (world as RayTracerWorld).canvas;
    final color = colorFromString(input1);
    for (int row = 0; row < canvas.height; row++) {
      for (int column = 0; column < canvas.width; column++) {
        final actual = canvas.getPixel(column, row);
        expectMatch(actual.equals(color), true);
      }
    }
  }
}

class ThenPixelIsRed extends Given1<String> {
  @override
  Pattern get pattern => RegExp(r"pixel_at\(([^)]+)\) = red");

  @override
  Future<void> executeStep(input1) async {
    final w = world as RayTracerWorld;
    final elements = input1.split(',');
    final column = int.parse(elements[1]);
    final row = int.parse(elements[2]);
    expectMatch(w.canvas.getPixel(column, row).equals(w.red), true);
  }
}

StepDefinitionGeneric thenPpmLines1to3Equal() =>
    given1<String, RayTracerWorld>('Then lines 1-3 of ppm are',
        (text, context) async {
      final expectedLines = text.split('\n');
      final actualLines = context.world.canvas.toPPMString().split('\n');
      for (int i = 0; i < 3; i++) {
        context.expectMatch(actualLines[i], expectedLines[i]);
      }
    });

StepDefinitionGeneric thenPpmLines4to6Equal() =>
    given1<String, RayTracerWorld>('Then lines 4-6 of ppm are',
        (text, context) async {
      final expectedLines = text.split('\n');
      final actualLines = context.world.canvas.toPPMString().split('\n');
      for (int i = 0; i < 3; i++) {
        context.expectMatch(actualLines[i + 3], expectedLines[i]);
      }
    });

StepDefinitionGeneric thenPpmLines4to7Equal() =>
    given1<String, RayTracerWorld>('Then lines 4-7 of ppm are',
        (text, context) async {
      final expectedLines = text.split('\n');
      final actualLines = context.world.canvas.toPPMString().split('\n');
      for (int i = 0; i < 4; i++) {
        context.expectMatch(actualLines[i + 3], expectedLines[i]);
      }
    });

StepDefinitionGeneric thenPpmEndsWithNewLine() =>
    given<RayTracerWorld>('Then ppm ends with a newline character',
        (context) async {
      final ppm = context.world.canvas.toPPMString();
      context.expectMatch(ppm[ppm.length - 1], '\n');
    });
