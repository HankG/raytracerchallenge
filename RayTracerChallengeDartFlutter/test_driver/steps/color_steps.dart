import 'package:gherkin/gherkin.dart';
import 'package:raytracerchallenge/model/color.dart';

import '../utils/string_utils.dart';
import '../worlds/raytracer.world.dart';
import 'tuple_steps.dart';

final colorSteps = [
  GivenColorC(),
  GivenColorC1(),
  GivenColorC2(),
  GivenColorC3(),
  thenCRedFieldEquals(),
  thenCGreenFieldEquals(),
  thenCBlueFieldEquals(),
  ThenC1PlusC2BEquals(),
  ThenC1MinusC2BEquals(),
  ThenC1TimesC2BEquals(),
  ThenCScaledEquals(),
];

// Given

abstract class GivenColor<W> extends GivenTuple<W> {
  GivenColor(super.lead, {super.typeName = 'color'});

  void assignColor(W World, Color value);

  @override
  void assign(w, v) => throw Error();

  @override
  Future<void> executeStep(String input1) async {
    assignColor(world as W, colorFromString(input1));
  }
}

class GivenColorC extends GivenColor<RayTracerWorld> {
  GivenColorC() : super('c');

  @override
  void assignColor(w, value) => w.c = value;
}

class GivenColorC1 extends GivenColor<RayTracerWorld> {
  GivenColorC1() : super('c1');

  @override
  void assignColor(w, value) => w.c1 = value;
}

class GivenColorC2 extends GivenColor<RayTracerWorld> {
  GivenColorC2() : super('c2');

  @override
  void assignColor(w, value) => w.c2 = value;
}

class GivenColorC3 extends GivenColor<RayTracerWorld> {
  GivenColorC3() : super('c3');

  @override
  void assignColor(w, value) => w.c3 = value;
}

// Then

StepDefinitionGeneric thenCRedFieldEquals() =>
    given1<num, RayTracerWorld>('c.red = {num}', (value, context) async {
      context.expectMatch(context.world.c.red, value);
    });

StepDefinitionGeneric thenCGreenFieldEquals() =>
    given1<num, RayTracerWorld>('c.green = {num}', (value, context) async {
      context.expectMatch(context.world.c.green, value);
    });

StepDefinitionGeneric thenCBlueFieldEquals() =>
    given1<num, RayTracerWorld>('c.blue = {num}', (value, context) async {
      context.expectMatch(context.world.c.blue, value);
    });

class ThenC1PlusC2BEquals extends Given1<String> {
  @override
  RegExp get pattern => RegExp(r"c1 \+ c2 = color\(([^)]+)\)");

  @override
  Future<void> executeStep(String input1) async {
    final w = world as RayTracerWorld;
    final actual = w.c1 + w.c2;
    final expected = colorFromString(input1);
    expectMatch(actual.equals(expected), true);
  }
}

class ThenC1MinusC2BEquals extends Given1<String> {
  @override
  RegExp get pattern => RegExp(r"c1 - c2 = color\(([^)]+)\)");

  @override
  Future<void> executeStep(String input1) async {
    final w = world as RayTracerWorld;
    final actual = w.c1 - w.c2;
    final expected = colorFromString(input1);
    expectMatch(actual.equals(expected, tolerance: 1e-12), true);
  }
}

class ThenCScaledEquals extends Given1<String> {
  @override
  RegExp get pattern => RegExp(r"c \* 2 = color\(([^)]+)\)");

  @override
  Future<void> executeStep(String input1) async {
    final w = world as RayTracerWorld;
    final actual = w.c.scale(2);
    final expected = colorFromString(input1);
    expectMatch(actual.equals(expected), true);
  }
}

class ThenC1TimesC2BEquals extends Given1<String> {
  @override
  RegExp get pattern => RegExp(r"c1 \* c2 = color\(([^)]+)\)");

  @override
  Future<void> executeStep(String input1) async {
    final w = world as RayTracerWorld;
    final actual = w.c1 * w.c2;
    final expected = colorFromString(input1);
    expectMatch(actual.equals(expected, tolerance: 1e-12), true);
  }
}
