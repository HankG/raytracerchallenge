import 'package:gherkin/gherkin.dart';
import 'package:raytracerchallenge/model/ray.dart';
import 'package:raytracerchallenge/model/tuple.dart';
import 'package:raytracerchallenge/utils/transforms.dart';

import '../worlds/raytracer.world.dart';
import 'steps_utils.dart';

final raySteps = [
  givenOrigin(),
  givenDirection(),
  givenRay(),
  givenTranslation(),
  givenScalingAndRotation(),
  givenScaling(),
  whenR2EqualsTransform(),
  thenROriginEqualsOrigin(),
  thenRDirectionEqualsDirection(),
  thenPositionEquals(),
  thenR2OriginEquals(),
  thenR2DirectionEquals(),
];

// Givens
StepDefinitionGeneric givenOrigin() =>
    given1<String, RayTracerWorld>(r'origin ← point\((.*)\)',
        (originData, context) async {
      final elements =
          strippedElements(originData).map((e) => num.parse(e)).toList();
      final origin = point(elements[0], elements[1], elements[2]);
      context.world.origin = origin;
      context.world.r = ray(origin, context.world.direction);
    });

StepDefinitionGeneric givenDirection() =>
    given1<String, RayTracerWorld>(r'direction ← vector\((.*)\)',
        (directionData, context) async {
      final elements =
          strippedElements(directionData).map((e) => num.parse(e)).toList();
      final direction = vector(elements[0], elements[1], elements[2]);
      context.world.direction = direction;
      context.world.r = ray(context.world.origin, direction);
    });

StepDefinitionGeneric givenRay() =>
    given1<String, RayTracerWorld>(r'r ← ray\((.*)\)',
        (rayData, context) async {
      final rayDataParts = strippedElements(rayData);
      if (rayDataParts.first == 'origin' && rayDataParts.last == 'direction') {
        return;
      }
      final elements = rayDataParts.map((e) => num.parse(e)).toList();
      final origin = point(elements[0], elements[1], elements[2]);
      final direction = vector(elements[3], elements[4], elements[5]);
      context.world.r = ray(origin, direction);
    });

StepDefinitionGeneric givenTranslation() =>
    given1<String, RayTracerWorld>(r'm ← translation\((.*)\)',
        (translationData, context) async {
      final elements =
          strippedElements(translationData).map((e) => num.parse(e)).toList();
      context.world.transform =
          translate(elements[0], elements[1], elements[2]);
    });

StepDefinitionGeneric givenScalingAndRotation() =>
    given2<String, String, RayTracerWorld>(
        r'm ← scaling\((.*)\) \* rotation(.*)\)',
        (scalingData, rotationData, context) async {
      final scalingElements =
          strippedElements(scalingData).map((e) => interpretNumber(e)).toList();
      final direction = rotationData[1];
      final angleText = rotationData.substring(3);
      final angle = interpretNumber(angleText);
      final scaling =
          scale(scalingElements[0], scalingElements[1], scalingElements[2]);
      final rotation = switch (direction) {
        'x' => rotateX(angle),
        'y' => rotateY(angle),
        'z' => rotateZ(angle),
        _ => throw Exception('Unknown rotation axis: $direction')
      };
      context.world.transform = scaling * rotation;
    });

StepDefinitionGeneric givenScaling() =>
    given1<String, RayTracerWorld>(r'm ← scaling\((.*)\)',
        (scalingData, context) async {
      final elements =
          strippedElements(scalingData).map((e) => interpretNumber(e)).toList();
      context.world.transform = scale(elements[0], elements[1], elements[2]);
    });

// Whens
StepDefinitionGeneric whenR2EqualsTransform() =>
    given<RayTracerWorld>(r'r2 ← transform\(r, m\)', (context) async {
      context.world.r2 = context.world.r.transform(context.world.transform);
    });

// Thens
StepDefinitionGeneric thenROriginEqualsOrigin() =>
    given<RayTracerWorld>('r.origin = origin', (context) async {
      context.expectMatch(context.world.origin, context.world.r.origin);
    });

StepDefinitionGeneric thenRDirectionEqualsDirection() =>
    given<RayTracerWorld>('r.direction = direction', (context) async {
      context.expectMatch(context.world.direction, context.world.r.direction);
    });

StepDefinitionGeneric thenPositionEquals() =>
    given2<String, String, RayTracerWorld>(r'position\((.*)\) = point\((.*)\)',
        (positionData, expectedData, context) async {
      final posElements = strippedElements(positionData);
      if (posElements.first != 'r') {
        throw Exception('Only know how to assign to world Ray variable "r"');
      }
      final t = num.parse(posElements[1]);
      final em =
          strippedElements(expectedData).map((e) => num.parse(e)).toList();
      final expected = point(em[0], em[1], em[2]);
      final actual = context.world.r.position(t);
      context.expectMatch(actual, expected);
    });

StepDefinitionGeneric thenR2OriginEquals() =>
    given1<String, RayTracerWorld>(r'r2.origin = point\((.*)\)',
        (expectedData, context) async {
      final em =
          strippedElements(expectedData).map((e) => num.parse(e)).toList();
      final expected = point(em[0], em[1], em[2]);
      context.expectMatch(context.world.r2.origin, expected);
    });

StepDefinitionGeneric thenR2DirectionEquals() =>
    given1<String, RayTracerWorld>(r'r2.direction = vector\((.*)\)',
        (expectedData, context) async {
      final em =
          strippedElements(expectedData).map((e) => num.parse(e)).toList();
      final expected = vector(em[0], em[1], em[2]);
      context.expectMatch(context.world.r2.direction, expected);
    });
