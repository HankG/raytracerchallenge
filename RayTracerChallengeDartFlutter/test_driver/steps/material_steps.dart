import 'package:gherkin/gherkin.dart';
import 'package:raytracerchallenge/model/color.dart';
import 'package:raytracerchallenge/model/surface_material.dart';
import 'package:raytracerchallenge/utils/lighting.dart';

import '../utils/string_utils.dart';
import '../worlds/raytracer.world.dart';
import 'steps_utils.dart';
import 'tuple_steps.dart';

final materialSteps = [
  givenLight(),
  givenAmbientValue(),
  GivenEyeVector(),
  GivenNormalVector(),
  whenLightingCalculated(),
  thenMaterialColorEquals(),
  thenAmbientValueEquals(),
  thenDiffuseValueEquals(),
  thenSpecularValueEquals(),
  thenShininessValueEquals(),
  thenResultColorEquals(),
];

// Givens
StepDefinitionGeneric givenLight() =>
    given<RayTracerWorld>(r'm ← material\(\)', (context) async {
      context.world.surfaceMaterial = material();
    });

StepDefinitionGeneric givenAmbientValue() =>
    given1<num, RayTracerWorld>(r'm.ambient ← {num}', (ambient, context) async {
      context.world.surfaceMaterial =
          context.world.surfaceMaterial.copyWith(ambient: ambient);
    });

class GivenEyeVector extends GivenVector<RayTracerWorld> {
  GivenEyeVector() : super('eyev');

  @override
  void assign(w, value) => w.eyeV = value;
}

class GivenNormalVector extends GivenVector<RayTracerWorld> {
  GivenNormalVector() : super('normalv');

  @override
  void assign(w, value) => w.normal = value;
}

// Whens
StepDefinitionGeneric whenLightingCalculated() => given<RayTracerWorld>(
      r'result ← lighting\(m, light, position, eyev, normalv\)',
      (context) async {
        final result = lighting(
          context.world.surfaceMaterial,
          context.world.light,
          context.world.p,
          context.world.eyeV,
          context.world.normal,
        );
        context.world.lightCalcResult = result;
      },
    );

// Thens
StepDefinitionGeneric thenMaterialColorEquals() =>
    given1<String, RayTracerWorld>(r'm.color = color\((.*)\)',
        (colorData, context) async {
      final elements =
          strippedElements(colorData).map((e) => num.parse(e)).toList();
      final expected = Color(elements[0], elements[1], elements[2]);
      context.expectMatch(context.world.surfaceMaterial.color, expected);
    });

StepDefinitionGeneric thenAmbientValueEquals() =>
    given1<num, RayTracerWorld>(r'm.ambient = {num}',
        (expected, context) async {
      context.expectMatch(context.world.surfaceMaterial.ambient, expected);
    });

StepDefinitionGeneric thenDiffuseValueEquals() =>
    given1<num, RayTracerWorld>(r'm.diffuse = {num}',
        (expected, context) async {
      context.expectMatch(context.world.surfaceMaterial.diffuse, expected);
    });

StepDefinitionGeneric thenSpecularValueEquals() =>
    given1<num, RayTracerWorld>(r'm.specular = {num}',
        (expected, context) async {
      context.expectMatch(context.world.surfaceMaterial.specular, expected);
    });

StepDefinitionGeneric thenShininessValueEquals() =>
    given1<num, RayTracerWorld>(r'm.shininess = {num}',
        (expected, context) async {
      context.expectMatch(context.world.surfaceMaterial.shininess, expected);
    });

StepDefinitionGeneric thenResultColorEquals() =>
    given1<String, RayTracerWorld>(r'result = color\((.*)\)',
        (colorData, context) async {
      final expected = colorFromString(colorData);
      context.expectMatch(
          context.world.lightCalcResult.equals(expected, tolerance: 1e-5),
          true);
    });
