import 'package:gherkin/gherkin.dart';
import 'package:raytracerchallenge/model/color.dart';
import 'package:raytracerchallenge/model/light.dart';
import 'package:raytracerchallenge/model/tuple.dart';

import '../worlds/raytracer.world.dart';
import 'steps_utils.dart';

final lightsSteps = [
  givenLightIntensity(),
  givenLightPosition(),
  givenPointLightDefinition(),
  thenLightPositionEquals(),
  thenLightIntensityEquals(),
];

// Given
StepDefinitionGeneric givenLightIntensity() =>
    given1<String, RayTracerWorld>(r'intensity ← color\((.*)\)',
        (lightColorData, context) async {
      final elements = strippedElements(lightColorData)
          .map((e) => interpretNumber(e))
          .toList();
      context.world.c = Color(elements[0], elements[1], elements[2]);
    });

StepDefinitionGeneric givenLightPosition() =>
    given1<String, RayTracerWorld>(r'position ← point\((.*)\)',
        (lightColorData, context) async {
      final elements = strippedElements(lightColorData)
          .map((e) => interpretNumber(e))
          .toList();
      context.world.p = point(elements[0], elements[1], elements[2]);
    });

StepDefinitionGeneric givenPointLightDefinition() =>
    given1<String, RayTracerWorld>(r'light ← point_light\((.*)\)',
        (pointLightData, context) async {
      final elements = strippedElements(pointLightData);
      if (elements.first == 'position' && elements.last == 'intensity') {
        context.world.light = pointLight(context.world.c, context.world.p);
        return;
      }
      final rdp = elements.map((e) => interpretNumber(e)).toList();
      final position = point(rdp[0], rdp[1], rdp[2]);
      final c = color(rdp[3], rdp[4], rdp[5]);
      context.world.light = pointLight(c, position);
      // final elements = rayDataParts.map((e) => num.parse(e)).toList();
      // final origin = point(elements[0], elements[1], elements[2]);
      // final direction = vector(elements[3], elements[4], elements[5]);
      // context.world.r = ray(origin, direction);
    });

// Then
StepDefinitionGeneric thenLightPositionEquals() =>
    given<RayTracerWorld>(r'light.position = position', (context) async {
      context.expectMatch(context.world.light.position, context.world.p);
    });

StepDefinitionGeneric thenLightIntensityEquals() =>
    given<RayTracerWorld>(r'light.intensity = intensity', (context) async {
      context.expectMatch(context.world.light.intensity, context.world.c);
    });
