import 'dart:math';

import 'package:gherkin/gherkin.dart';
import 'package:raytracerchallenge/model/tuple.dart';
import 'package:raytracerchallenge/utils/transforms.dart';

import '../utils/string_utils.dart';
import '../worlds/raytracer.world.dart';
import 'steps_utils.dart';

final tupleSteps = [
  GivenTupleA(),
  GivenTupleA1(),
  GivenTupleA2(),
  GivenTupleB(),
  GivenPointP(),
  GivenPointP1(),
  GivenPointP2(),
  GivenVectorV(),
  GivenVectorV1(),
  GivenVectorV2(),
  GivenVectorZero(),
  GivenVectorA(),
  GivenVectorB(),
  GivenNormalVector(),
  whenNormOfV(),
  whenRIsReflectedTuple(),
  thenAxEquals(),
  thenAyEquals(),
  thenAzEquals(),
  thenAwEquals(),
  thenAisPoint(),
  thenAisNotPoint(),
  thenAisVector(),
  thenAisNotVector(),
  ThenPEqualsTuple(),
  ThenVEqualsTuple(),
  ThenA1PlusA2EqualsTuple(),
  ThenP1MinusP2EqualsVector(),
  ThenPMinusVEqualsPoint(),
  ThenVMinusV2EqualsVector(),
  ThenZeroMinusVEqualsVector(),
  ThenNegativeAEqualsTuple(),
  ThenATupleTimesScalarEqualsTuple(),
  ThenATupleDivideByScalarEqualsTuple(),
  thenMagnitudeVEquals(),
  thenMagnitudeVEqualsSqrt(),
  ThenNormalizedVectorEquals(),
  ThenNormalizedVectorApproxEquals(),
  thenMagnitudeNorm(),
  thenDotABEquals(),
  ThenACrossBEquals(),
  ThenBCrossAEquals(),
  thenREqualsVector(),
];

// Givens
abstract class GivenTuple<W> extends Given1<String> {
  final String lead;
  final String typeName;
  final _exp = r"\(([^)]+)\)";

  GivenTuple(this.lead, {this.typeName = 'tuple'});

  @override
  RegExp get pattern => RegExp('$lead ← $typeName$_exp');

  void assign(W world, Tuple value);

  @override
  Future<void> executeStep(String input1) async {
    assign(world as W, tupleFromString(input1));
  }
}

abstract class GivenPoint<W> extends GivenTuple<W> {
  GivenPoint(super.lead, {super.typeName = 'point'});

  @override
  Future<void> executeStep(String input1) async {
    assign(world as W, pointFromString(input1));
  }
}

abstract class GivenVector<W> extends GivenTuple<W> {
  GivenVector(super.lead, {super.typeName = 'vector'});

  @override
  Future<void> executeStep(String input1) async {
    assign(world as W, vectorFromString(input1));
  }
}

class GivenTupleA extends GivenTuple<RayTracerWorld> {
  GivenTupleA() : super('a');

  @override
  void assign(w, tuple) => w.a = tuple;
}

class GivenTupleB extends GivenTuple<RayTracerWorld> {
  GivenTupleB() : super('b');

  @override
  void assign(w, tuple) => w.b = tuple;
}

class GivenTupleA1 extends GivenTuple<RayTracerWorld> {
  GivenTupleA1() : super('a1');

  @override
  void assign(w, tuple) => w.a1 = tuple;
}

class GivenTupleA2 extends GivenTuple<RayTracerWorld> {
  GivenTupleA2() : super('a2');

  @override
  void assign(w, tuple) => w.a2 = tuple;
}

class GivenPointP extends GivenPoint<RayTracerWorld> {
  GivenPointP() : super('p');

  @override
  void assign(w, tuple) => w.p = tuple;
}

class GivenPointP1 extends GivenPoint<RayTracerWorld> {
  GivenPointP1() : super('p1');

  @override
  void assign(w, tuple) => w.p1 = tuple;
}

class GivenPointP2 extends GivenPoint<RayTracerWorld> {
  GivenPointP2() : super('p2');

  @override
  void assign(w, value) => w.p2 = value;
}

class GivenVectorV extends GivenVector<RayTracerWorld> {
  GivenVectorV() : super('v');

  @override
  void assign(w, value) => w.v = value;
}

class GivenVectorV1 extends GivenVector<RayTracerWorld> {
  GivenVectorV1() : super('v1');

  @override
  void assign(w, value) => w.v1 = value;
}

class GivenVectorV2 extends GivenVector<RayTracerWorld> {
  GivenVectorV2() : super('v2');

  @override
  void assign(w, value) => w.v2 = value;
}

class GivenVectorZero extends GivenVector<RayTracerWorld> {
  GivenVectorZero() : super('zero');

  @override
  void assign(w, value) => w.v2 = value;
}

class GivenVectorA extends GivenVector<RayTracerWorld> {
  GivenVectorA() : super('a');

  @override
  void assign(w, value) => w.a = value;
}

class GivenVectorB extends GivenVector<RayTracerWorld> {
  GivenVectorB() : super('b');

  @override
  void assign(w, value) => w.b = value;
}

class GivenNormalVector extends GivenVector<RayTracerWorld> {
  GivenNormalVector() : super('n');

  @override
  void assign(RayTracerWorld world, Tuple value) => world.normal = value;
}

// Whens
StepDefinitionGeneric whenNormOfV() =>
    given<RayTracerWorld>(r'norm ← normalize\(v\)', (context) async {
      context.world.norm = context.world.v.normalize();
    });

StepDefinitionGeneric whenRIsReflectedTuple() =>
    given<RayTracerWorld>(r'r ← reflect\(v, n\)', (context) async {
      context.world.reflected = context.world.v.reflect(context.world.normal);
    });

// Thens

StepDefinitionGeneric thenAxEquals() => given1<num, RayTracerWorld>(
      'a.x = {num}',
      (ax, context) async {
        context.expectMatch(ax, context.world.a.x);
      },
    );

StepDefinitionGeneric thenAyEquals() => given1<num, RayTracerWorld>(
      'a.y = {num}',
      (ay, context) async {
        context.expectMatch(ay, context.world.a.y);
      },
    );

StepDefinitionGeneric thenAzEquals() => given1<num, RayTracerWorld>(
      'a.z = {num}',
      (az, context) async {
        context.expectMatch(az, context.world.a.z);
      },
    );

StepDefinitionGeneric thenAwEquals() => given1<num, RayTracerWorld>(
      'a.w = {num}',
      (aw, context) async {
        context.expectMatch(aw, context.world.a.w);
      },
    );

StepDefinitionGeneric thenAisPoint() => given<RayTracerWorld>(
      'And a is a point',
      (context) async {
        context.expectMatch(context.world.a.isPoint, true);
      },
    );

StepDefinitionGeneric thenAisNotPoint() => given<RayTracerWorld>(
      'And a is not a point',
      (context) async {
        context.expectMatch(context.world.a.isPoint, false);
      },
    );

StepDefinitionGeneric thenAisVector() => given<RayTracerWorld>(
      'And a is a vector',
      (context) async {
        context.expectMatch(context.world.a.isVector, true);
      },
    );

StepDefinitionGeneric thenAisNotVector() => given<RayTracerWorld>(
      'And a is not a vector',
      (context) async {
        context.expectMatch(context.world.a.isVector, false);
      },
    );

class ThenPEqualsTuple extends Given1<String> {
  @override
  RegExp get pattern => RegExp(r"p = tuple\(([^)]+)\)");

  @override
  Future<void> executeStep(String input1) async {
    final w = world as RayTracerWorld;
    final expected = tupleFromString(input1);
    expectMatch(w.p.equals(expected), true);
  }
}

class ThenVEqualsTuple extends Given1<String> {
  @override
  RegExp get pattern => RegExp(r"v = tuple\(([^)]+)\)");

  @override
  Future<void> executeStep(String input1) async {
    final w = world as RayTracerWorld;
    final expected = tupleFromString(input1);
    expectMatch(w.v.equals(expected), true);
  }
}

class ThenA1PlusA2EqualsTuple extends Given1<String> {
  @override
  RegExp get pattern => RegExp(r"a1 \+ a2 = tuple\(([^)]+)\)");

  @override
  Future<void> executeStep(String input1) async {
    final w = world as RayTracerWorld;
    final expected = tupleFromString(input1);
    final sum = w.a1 + w.a2;
    expectMatch(sum.equals(expected), true);
  }
}

class ThenP1MinusP2EqualsVector extends Given1<String> {
  @override
  RegExp get pattern => RegExp(r"p1 - p2 = vector\(([^)]+)\)");

  @override
  Future<void> executeStep(String input1) async {
    final w = world as RayTracerWorld;
    final expected = vectorFromString(input1);
    final result = w.p1 - w.p2;
    expectMatch(result.isVector, true);
    expectMatch(result.equals(expected), true);
  }
}

class ThenPMinusVEqualsPoint extends Given1<String> {
  @override
  RegExp get pattern => RegExp(r"p - v = point\(([^)]+)\)");

  @override
  Future<void> executeStep(String input1) async {
    final w = world as RayTracerWorld;
    final expected = pointFromString(input1);
    final result = w.p - w.v;
    expectMatch(result.isPoint, true);
    expectMatch(result.equals(expected), true);
  }
}

class ThenVMinusV2EqualsVector extends Given1<String> {
  @override
  RegExp get pattern => RegExp(r"v1 - v2 = vector\(([^)]+)\)");

  @override
  Future<void> executeStep(String input1) async {
    final w = world as RayTracerWorld;
    final expected = vectorFromString(input1);
    final result = w.v1 - w.v2;
    expectMatch(result.isVector, true);
    expectMatch(result.equals(expected), true);
  }
}

class ThenZeroMinusVEqualsVector extends Given1<String> {
  @override
  RegExp get pattern => RegExp(r"zero - v = vector\(([^)]+)\)");

  @override
  Future<void> executeStep(String input1) async {
    final w = world as RayTracerWorld;
    final expected = vectorFromString(input1);
    final result = w.zero - w.v;
    expectMatch(result.isVector, true);
    expectMatch(result.equals(expected), true);
  }
}

class ThenNegativeAEqualsTuple extends Given1<String> {
  @override
  RegExp get pattern => RegExp(r"-a = tuple\(([^)]+)\)");

  @override
  Future<void> executeStep(String input1) async {
    final w = world as RayTracerWorld;
    final expected = tupleFromString(input1);
    final result = -w.a;
    expectMatch(result.equals(expected), true);
  }
}

class ThenATupleTimesScalarEqualsTuple extends Given2<num, String> {
  @override
  RegExp get pattern => RegExp(r"a \* {num} = tuple\(([^)]+)\)");

  @override
  Future<void> executeStep(num scalar, String input1) async {
    final w = world as RayTracerWorld;
    final expected = tupleFromString(input1);
    final Tuple result = w.a * scalar;
    expectMatch(result.equals(expected), true);
  }
}

class ThenATupleDivideByScalarEqualsTuple extends Given2<num, String> {
  @override
  RegExp get pattern => RegExp(r"a / {num} = tuple\(([^)]+)\)");

  @override
  Future<void> executeStep(num scalar, String input1) async {
    final w = world as RayTracerWorld;
    final expected = tupleFromString(input1);
    final Tuple result = w.a / scalar;
    expectMatch(result.equals(expected), true);
  }
}

StepDefinitionGeneric thenMagnitudeVEquals() => given1<String, RayTracerWorld>(
      r"magnitude\(v\) = {num}",
      (mag, context) async {
        context.expectMatch(context.world.v.magnitude, num.parse(mag));
      },
    );

StepDefinitionGeneric thenMagnitudeVEqualsSqrt() =>
    given1<String, RayTracerWorld>(
      r"magnitude\(v\) = √{num}",
      (mag, context) async {
        context.expectMatch(context.world.v.magnitude, sqrt(num.parse(mag)));
      },
    );

class ThenNormalizedVectorEquals extends Given1<String> {
  @override
  RegExp get pattern => RegExp(r"normalize\(v\) = vector\(([^)]+)\)");

  @override
  Future<void> executeStep(String input1) async {
    final w = world as RayTracerWorld;
    final expected = vectorFromString(input1);
    expectMatch(w.v.normalize().equals(expected), true);
  }
}

class ThenNormalizedVectorApproxEquals extends Given1<String> {
  @override
  RegExp get pattern =>
      RegExp(r"normalize\(v\) = approximately vector\(([^)]+)\)");

  @override
  Future<void> executeStep(String input1) async {
    final w = world as RayTracerWorld;
    final expected = vectorFromString(input1);
    expectMatch(w.v.normalize().equals(expected, tolerance: 1e-5), true);
  }
}

StepDefinitionGeneric thenMagnitudeNorm() => given1<String, RayTracerWorld>(
      r"magnitude\(norm\) = {num}",
      (mag, context) async {
        context.expectMatch(context.world.norm.magnitude, num.parse(mag));
      },
    );

StepDefinitionGeneric thenDotABEquals() => given1<String, RayTracerWorld>(
      r"dot\(a\, b\) = {num}",
      (mag, context) async {
        final expected = num.parse(mag);
        final actual = context.world.a.dot(context.world.b);
        context.expectMatch(actual, expected);
      },
    );

class ThenACrossBEquals extends Given1<String> {
  @override
  RegExp get pattern => RegExp(r"cross\(a, b\) = vector\(([^)]+)\)");

  @override
  Future<void> executeStep(String input1) async {
    final w = world as RayTracerWorld;
    final actual = w.a.cross(w.b);
    final expected = vectorFromString(input1);
    expectMatch(actual.equals(expected), true);
  }
}

class ThenBCrossAEquals extends Given1<String> {
  @override
  RegExp get pattern => RegExp(r"cross\(b, a\) = vector\(([^)]+)\)");

  @override
  Future<void> executeStep(String input1) async {
    final w = world as RayTracerWorld;
    final actual = w.b.cross(w.a);
    final expected = vectorFromString(input1);
    expectMatch(actual.equals(expected), true);
  }
}

StepDefinitionGeneric thenREqualsVector() =>
    given1<String, RayTracerWorld>(r'r = vector\((.*)\)',
        (expectedData, context) async {
      final elements =
          strippedElements(expectedData).map((e) => num.parse(e)).toList();
      final expected = vector(elements[0], elements[1], elements[2]);
      context.expectMatch(context.world.reflected, expected);
    });
