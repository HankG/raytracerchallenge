import 'package:gherkin/gherkin.dart';
import 'package:raytracerchallenge/model/matrix.dart';

import '../utils/string_utils.dart';
import '../worlds/raytracer.world.dart';

final matricesSteps = [
  given2x2Matrix(),
  given2x2MatrixA(),
  given3x3Matrix(),
  given3x3MatrixA(),
  given4x4Matrix(),
  given4x4MatrixA(),
  given4x4MatrixB(),
  givenMatrixA(),
  givenMatrixB(),
  givenAIsTransposeIdentity(),
  givenBSubmatrixofA(),
  givenBIsInverseOfA(),
  givenCMatrixEqualsATimesB(),
  thenMatrixElementEquals(),
  thenMatrixBElementEqualsFraction(),
  thenAMatrixEqualsBMatrix(),
  thenAMatrixNotEqualsBMatrix(),
  thenAMatrixTimesBMatrixEquals(),
  ThenAMatrixTimesBTupleEquals(),
  thenAMatrixTimesIdentityEqualsA(),
  thenIdentityMatrixTimesATupleEqualsA(),
  thenTransposeAEquals(),
  thenAIsIdentityMatrix(),
  thenDetAEquals(),
  thenDetBEquals(),
  thenA02Equals(),
  thenA21Equals(),
  thenMinorOfAEquals(),
  thenCofactorOfAEquals(),
  thenAIsInvertible(),
  thenAIsNotInvertible(),
  thenBMatrixEquals(),
  thenAInverseEquals(),
  thenCTimesInverseBEqualsA(),
];

// Givens
StepDefinitionGeneric given2x2Matrix() =>
    given1<GherkinTable, RayTracerWorld>(' the following 2x2 matrix M',
        (table, context) async {
      final matrix = Matrix.new2x2();
      fillMatrixFromTable(matrix, table);
      context.world.matrixM = matrix;
    });

StepDefinitionGeneric given2x2MatrixA() =>
    given1<GherkinTable, RayTracerWorld>(' the following 2x2 matrix A',
        (table, context) async {
      final matrix = Matrix.new2x2();
      fillMatrixFromTable(matrix, table);
      context.world.matrixA = matrix;
    });

StepDefinitionGeneric given3x3Matrix() =>
    given1<GherkinTable, RayTracerWorld>(' the following 3x3 matrix M',
        (table, context) async {
      final matrix = Matrix.new3x3();
      fillMatrixFromTable(matrix, table);
      context.world.matrixM = matrix;
    });

StepDefinitionGeneric given3x3MatrixA() =>
    given1<GherkinTable, RayTracerWorld>(' the following 3x3 matrix A',
        (table, context) async {
      final matrix = Matrix.new3x3();
      fillMatrixFromTable(matrix, table);
      context.world.matrixA = matrix;
    });

StepDefinitionGeneric given4x4Matrix() =>
    given1<GherkinTable, RayTracerWorld>(' the following 4x4 matrix M',
        (table, context) async {
      final matrix = Matrix.new4x4();
      fillMatrixFromTable(matrix, table);
      context.world.matrixM = matrix;
    });

StepDefinitionGeneric given4x4MatrixA() =>
    given1<GherkinTable, RayTracerWorld>(' the following 4x4 matrix A',
        (table, context) async {
      final matrix = Matrix.new4x4();
      fillMatrixFromTable(matrix, table);
      context.world.matrixA = matrix;
    });

StepDefinitionGeneric given4x4MatrixB() =>
    given1<GherkinTable, RayTracerWorld>(' the following 4x4 matrix B',
        (table, context) async {
      final matrix = Matrix.new4x4();
      fillMatrixFromTable(matrix, table);
      context.world.matrixB = matrix;
    });

StepDefinitionGeneric givenMatrixA() =>
    given1<GherkinTable, RayTracerWorld>('the following matrix A:',
        (table, context) async {
      final matrix = Matrix.new4x4();
      fillMatrixFromTable(matrix, table);
      context.world.matrixA = matrix;
    });

StepDefinitionGeneric givenMatrixB() =>
    given1<GherkinTable, RayTracerWorld>('the following matrix B:',
        (table, context) async {
      final matrix = Matrix.new4x4();
      fillMatrixFromTable(matrix, table);
      context.world.matrixB = matrix;
    });

StepDefinitionGeneric givenAIsTransposeIdentity() =>
    given<RayTracerWorld>(r'Given A ← transpose\(identity_matrix\)',
        (context) async {
      context.world.matrixA = Matrix.identity().transpose();
    });

StepDefinitionGeneric givenBIsInverseOfA() =>
    given<RayTracerWorld>(r'B ← inverse\(A\)', (context) async {
      context.world.matrixB = context.world.matrixA.invert();
    });

StepDefinitionGeneric givenCMatrixEqualsATimesB() =>
    given<RayTracerWorld>(r'And C ← A \* B', (context) async {
      context.world.matrixC = context.world.matrixA * context.world.matrixB;
    });
StepDefinitionGeneric givenBSubmatrixofA() =>
    given1<String, RayTracerWorld>(r'B ← submatrix\(([^)]+)\)',
        (indicesString, context) async {
      final elements = indicesString.split(',');
      if (elements.first.trim() != 'A') {
        throw Exception('Expecting matrix A requested');
      }
      final row = int.parse(elements[1]);
      final column = int.parse(elements[2]);
      context.world.matrixB = context.world.matrixA.subMatrix(row, column);
    });

// Whens

// Thens
StepDefinitionGeneric thenMatrixElementEquals() =>
    given3<int, int, num, RayTracerWorld>(r'M\[{int},{int}\] = {num}',
        (row, column, value, context) async {
      context.expect(value, context.world.matrixM.getValue(row, column));
    });

StepDefinitionGeneric thenMatrixBElementEqualsFraction() =>
    given4<int, int, num, num, RayTracerWorld>(
        r'B\[{int},{int}\] = {num}/{num}',
        (row, column, numerator, denominator, context) async {
      context.expect(
          numerator / denominator, context.world.matrixB.getValue(row, column));
    });

StepDefinitionGeneric thenAMatrixEqualsBMatrix() =>
    given<RayTracerWorld>('A = B', (context) async {
      context.expectMatch(
          context.world.matrixA.equals(context.world.matrixB), true);
    });

StepDefinitionGeneric thenAMatrixNotEqualsBMatrix() =>
    given<RayTracerWorld>('A != B', (context) async {
      context.expectMatch(
          context.world.matrixA.equals(context.world.matrixB), false);
    });

StepDefinitionGeneric thenAMatrixTimesBMatrixEquals() =>
    given1<GherkinTable, RayTracerWorld>(r'A \* B is the following 4x4 matrix',
        (table, context) async {
      final expected = Matrix.new4x4();
      fillMatrixFromTable(expected, table);
      final actual = context.world.matrixA * context.world.matrixB;
      context.expectMatch(actual.equals(expected), true);
    });

class ThenAMatrixTimesBTupleEquals extends Given1<String> {
  @override
  RegExp get pattern => RegExp(r"A \* b = tuple\(([^)]+)\)");

  @override
  Future<void> executeStep(String input1) async {
    final w = world as RayTracerWorld;
    final actual = w.matrixA.times(w.b);
    final expected = tupleFromString(input1);
    expectMatch(actual.equals(expected), true);
  }
}

StepDefinitionGeneric thenAMatrixTimesIdentityEqualsA() =>
    given<RayTracerWorld>(r'A \* identity_matrix = A', (context) async {
      final actual = context.world.matrixA * Matrix.identity();
      context.expectMatch(actual.equals(context.world.matrixA), true);
    });

StepDefinitionGeneric thenIdentityMatrixTimesATupleEqualsA() =>
    given<RayTracerWorld>(r'identity_matrix \* a = a', (context) async {
      final actual = Matrix.identity().times(context.world.a);
      context.expectMatch(actual.equals(context.world.a), true);
    });

//
StepDefinitionGeneric thenTransposeAEquals() =>
    given1<GherkinTable, RayTracerWorld>(
        r'transpose\(A\) is the following matrix', (table, context) async {
      final expected = Matrix.new4x4();
      fillMatrixFromTable(expected, table);
      final actual = context.world.matrixA.transpose();
      context.expectMatch(actual.equals(expected), true);
    });

StepDefinitionGeneric thenAIsIdentityMatrix() =>
    given<RayTracerWorld>('Then A = identity_matrix', (context) async {
      context.expectMatch(
          context.world.matrixA.equals(Matrix.identity()), true);
    });

StepDefinitionGeneric thenDetAEquals() =>
    given1<String, RayTracerWorld>(r'determinant\(A\) = {num}',
        (expected, context) async {
      final actual = context.world.matrixA.determinant();
      context.expectMatch(actual, num.parse(expected));
    });

StepDefinitionGeneric thenDetBEquals() =>
    given1<String, RayTracerWorld>(r'determinant\(B\) = {num}',
        (expected, context) async {
      final actual = context.world.matrixB.determinant();
      context.expectMatch(actual, num.parse(expected));
    });

StepDefinitionGeneric thenA02Equals() => given1<GherkinTable, RayTracerWorld>(
        r'submatrix\(A, 0, 2\) is the following 2x2 matrix',
        (table, context) async {
      final expected = Matrix.new2x2();
      fillMatrixFromTable(expected, table);
      final actual = context.world.matrixA.subMatrix(0, 2);
      context.expectMatch(actual.equals(expected), true);
    });

StepDefinitionGeneric thenA21Equals() => given1<GherkinTable, RayTracerWorld>(
        r'submatrix\(A, 2, 1\) is the following 3x3 matrix',
        (table, context) async {
      final expected = Matrix.new3x3();
      fillMatrixFromTable(expected, table);
      final actual = context.world.matrixA.subMatrix(2, 1);
      context.expectMatch(actual, expected);
      context.expectMatch(actual.equals(expected), true);
    });

StepDefinitionGeneric thenMinorOfAEquals() =>
    given2<String, num, RayTracerWorld>(r'minor\(([^)]+)\) = {num}',
        (indices, value, context) async {
      final elements = indices.split(',');
      if (elements.first.trim() != 'A') {
        throw Exception('Expecting A matrix to be requested');
      }
      final row = int.parse(elements[1]);
      final column = int.parse(elements[2]);
      context.expectMatch(context.world.matrixA.minor(row, column), value);
    });

StepDefinitionGeneric thenCofactorOfAEquals() =>
    given2<String, num, RayTracerWorld>(r'cofactor\(([^)]+)\) = {num}',
        (indices, value, context) async {
      final elements = indices.split(',');
      if (elements.first.trim() != 'A') {
        throw Exception('Expecting A matrix to be requested');
      }
      final row = int.parse(elements[1]);
      final column = int.parse(elements[2]);
      context.expectMatch(context.world.matrixA.cofactor(row, column), value);
    });

StepDefinitionGeneric thenAIsInvertible() =>
    given<RayTracerWorld>('And A is invertible', (context) async {
      context.expectMatch(context.world.matrixA.isInvertible, true);
    });

StepDefinitionGeneric thenAIsNotInvertible() =>
    given<RayTracerWorld>('And A is not invertible', (context) async {
      context.expectMatch(context.world.matrixA.isInvertible, false);
    });

StepDefinitionGeneric thenBMatrixEquals() =>
    given1<GherkinTable, RayTracerWorld>('B is the following 4x4 matrix',
        (table, context) async {
      final expected = Matrix.new4x4();
      fillMatrixFromTable(expected, table);
      context.expectMatch(
          context.world.matrixB.equals(expected, tolerance: 1e-5), true);
    });

StepDefinitionGeneric thenAInverseEquals() =>
    given1<GherkinTable, RayTracerWorld>(
        r'inverse\(A\) is the following 4x4 matrix', (table, context) async {
      final expected = Matrix.new4x4();
      fillMatrixFromTable(expected, table);
      context.expectMatch(
          context.world.matrixA.invert().equals(expected, tolerance: 1e-5),
          true);
    });

StepDefinitionGeneric thenCTimesInverseBEqualsA() =>
    given<RayTracerWorld>(r'C \* inverse\(B\) = A', (context) async {
      final actual = context.world.matrixC * context.world.matrixB.invert();
      context.expectMatch(
          actual.equals(context.world.matrixA, tolerance: 1e-12), true);
    });

void fillMatrixFromTable(Matrix matrix, GherkinTable table) {
  int rowCount = 0;
  int columnCount = 0;
  for (final column in table.header!.columns) {
    final value = num.parse(column ?? '');
    matrix.setValue(rowCount, columnCount, value);
    columnCount++;
  }
  rowCount++;
  for (final row in table.rows) {
    columnCount = 0;
    for (final column in row.columns) {
      final value = num.parse(column ?? '');
      matrix.setValue(rowCount, columnCount, value);
      columnCount++;
    }
    rowCount++;
  }
}
