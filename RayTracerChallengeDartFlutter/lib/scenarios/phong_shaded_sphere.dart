import '../model/color.dart';
import '../model/intersection.dart';
import '../model/light.dart';
import '../model/matrix.dart';
import '../model/ray.dart';
import '../model/sphere.dart';
import '../model/surface_material.dart';
import '../model/tuple.dart';
import '../utils.dart';
import '../utils/lighting.dart';
import '../utils/transforms.dart';
import 'pixel_stream_interface.dart';

class PhongShadedSphereScene implements IPixelStreamGenerator {
  final bool withDefaults;
  final int canvasSize;

  const PhongShadedSphereScene({
    required this.canvasSize,
    this.withDefaults = true,
  });

  @override
  Stream<PixelUpdate> generate() async* {
    const wallZ = 10;
    const wallSize = 7;
    const half = wallSize / 2;
    final pixelSize = wallSize / canvasSize;

    final shapeColor =
        withDefaults ? const Color(1, 0.2, 1) : const Color(1, 1, 1);
    final shapeTransform = withDefaults
        ? Matrix.identity()
        : Matrix.identity() *
            translate(0.5, 0.5, 3.0) *
            rotateZ(30.toRadians()) *
            scale(1.3, 0.8, 1.0);
    final shapeMaterial = withDefaults
        ? material(
            color: shapeColor,
          )
        : material(
            color: shapeColor,
            ambient: 0.05,
            diffuse: 0.8,
            specular: 0.5,
            shininess: 100.0,
          );
    final shape = Sphere(
      initTransform: shapeTransform,
      initMaterial: shapeMaterial,
    );
    final light = withDefaults
        ? pointLight(WHITE, point(-10, 10, -10))
        : pointLight(const Color(21.0 / 255.0, 181.0 / 255.0, 175.0 / 255.0),
            point(-10, 10, -10));
    final rayOrigin = point(0, 0, -7);

    final st = Stopwatch();
    st.start();
    for (int y = 0; y < canvasSize; y++) {
      final worldY = half - (pixelSize * y);
      for (int x = 0; x < canvasSize; x++) {
        final worldX = (pixelSize * x) - half;
        final wallPosition = point(worldX, worldY, wallZ);
        final direction = (wallPosition - rayOrigin).normalize();
        final r = ray(rayOrigin, direction);
        final intersections = shape.intersect(r);
        if (intersections.hit() != NO_INTERSECTION) {
          final t = intersections.xs.first.t;
          final p = r.position(t);
          final normal = shape.normal(p);
          final eye = -r.direction;
          final color = lighting(shape.surfaceMaterial, light, p, eye, normal);
          final update = PixelUpdate(x, y, color);
          yield update;
          if (st.elapsedMilliseconds > 100) {
            await Future.delayed(Duration(milliseconds: 10));
            st.reset();
          }
        }
      }
    }
  }
}
