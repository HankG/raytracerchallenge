import '../model/canvas.dart';
import '../model/color.dart';

Canvas solidBlueBuilder() {
  return Canvas(300, 200, fillColor: const Color(0, 0, 1));
}

Canvas solidRedBuilder() {
  return Canvas(300, 200, fillColor: const Color(1, 0, 0));
}

Canvas solidGreenBuilder() {
  return Canvas(300, 200, fillColor: const Color(0, 1, 0));
}
