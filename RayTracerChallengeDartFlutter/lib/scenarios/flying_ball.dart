import 'dart:math';

import '../model/canvas.dart';
import '../model/color.dart';
import '../model/tuple.dart';

class Projectile {
  final Tuple p;
  final Tuple v;

  const Projectile(this.p, this.v);

  Projectile update(Tuple gravity, Tuple wind) {
    final p2 = p + v;
    final v2 = v + gravity + wind;
    return Projectile(p2, v2);
  }

  String toString() => '${p.x},${p.y}';
}

Canvas flyingBallOutput() {
  const ballColor = Color(1, 0, 0);

  final gravity = vector(0, -0.1, 0);
  final wind = vector(0.1, 0, 0);
  var projectile = Projectile(
    point(0, 1, 0),
    vector(1, 3, 0).normalize() * 6,
  );

  int maxX = 0;
  int maxY = 0;
  final trajectory = <(num, num)>[];
  while (projectile.p.y >= 0) {
    print(projectile);
    if (projectile.p.x > maxX) {
      maxX = projectile.p.x.toInt();
    }

    if (projectile.p.y > maxY) {
      maxY = projectile.p.y.toInt();
    }

    projectile = projectile.update(gravity, wind);
    trajectory.add((projectile.p.x, projectile.p.y));
  }

  final xScale = maxX <= 298 ? 1 : 298 / maxX;
  final yScale = maxY <= 198 ? 1 : 198 / maxY;
  final scale = min(xScale, yScale);
  final canvas = Canvas(300, 200, fillColor: BLACK);
  trajectory
      .where((element) => element.$1 >= 0 && element.$2 >= 0)
      .map((e) => ((e.$1 * scale).round(), (e.$2 * scale).round()))
      .forEach((e) {
    canvas.setPixel(e.$1, 198 - e.$2, ballColor);
  });

  return canvas;
}
