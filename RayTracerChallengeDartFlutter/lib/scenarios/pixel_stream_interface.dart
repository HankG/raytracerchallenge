import '../model/color.dart';

class PixelUpdate {
  final int x;
  final int y;
  final Color color;

  PixelUpdate(this.x, this.y, this.color);

  @override
  String toString() {
    return 'PixelUpdate{x: $x, y: $y, color: $color}';
  }
}

abstract interface class IPixelStreamGenerator {
  Stream<PixelUpdate> generate();
}
