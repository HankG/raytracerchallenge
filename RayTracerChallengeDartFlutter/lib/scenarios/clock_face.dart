import 'dart:math';

import 'package:raytracerchallenge/model/color.dart';
import 'package:raytracerchallenge/utils/transforms.dart';

import '../model/canvas.dart';
import '../model/tuple.dart';

Canvas clockFace() {
  const size = 200;
  const length = 80;
  final canvas = Canvas(200, 200);

  final noon = point(0, -1, 0);
  final scaleMatrix = scale(length, length, length);
  final toCenter = translate(size / 2, size / 2, 0);

  for (double angle = 0; angle < 2 * pi; angle += pi / 6) {
    final pixel = (toCenter * scaleMatrix * rotateZ(angle)).times(noon);
    canvas.setPixel(pixel.x.toInt(), pixel.y.toInt(), WHITE);
  }

  return canvas;
}
