import 'package:raytracerchallenge/model/intersection.dart';

import '../model/canvas.dart';
import '../model/color.dart';
import '../model/ray.dart';
import '../model/sphere.dart';
import '../model/tuple.dart';

Canvas initialSphereRayCasting() {
  const canvasSize = 200;
  const wallZ = 10;
  const wallSize = 7;
  const half = wallSize / 2;
  const pixelSize = wallSize / canvasSize;
  final canvas = Canvas(canvasSize, canvasSize);
  const color = Color(1, 0, 0);

  final shape = Sphere();
  final rayOrigin = point(0, 0, -7);

  for (int y = 0; y < canvasSize; y++) {
    final worldY = half - (pixelSize * y);
    for (int x = 0; x < canvasSize; x++) {
      final worldX = (pixelSize * x) - half;
      final wallPosition = point(worldX, worldY, wallZ);
      final direction = (wallPosition - rayOrigin).normalize();
      final r = ray(rayOrigin, direction);
      final intersections = shape.intersect(r);
      if (intersections.hit() != NO_INTERSECTION) {
        canvas.setPixel(x, y, color);
      }
    }
  }
  return canvas;
}
