import 'dart:math';

import 'package:buffer_image/buffer_image.dart';
import 'package:flutter/material.dart';

import 'model/canvas.dart' as rtc;
import 'writers/canvas_buffer_image_writer.dart';

class CanvasViewerWidget extends StatelessWidget {
  final rtc.Canvas canvas;

  const CanvasViewerWidget({super.key, required this.canvas});

  @override
  Widget build(BuildContext context) {
    final image = canvas.buildBufferImage();
    final size = MediaQuery.of(context).size;
    final scaleX = image.width.toDouble() / size.width.toDouble();
    final scaleY = image.height.toDouble() / size.height.toDouble();
    final scale = max(scaleX, scaleY);
    return Image(
      image: RgbaImage.fromBufferImage(image, scale: scale),
    );
  }
}
