int colorValue(num value) {
  final rval = (value * 255).round();
  if (rval < 0) {
    return 0;
  }

  if (rval > 255) {
    return 255;
  }
  return rval;
}
