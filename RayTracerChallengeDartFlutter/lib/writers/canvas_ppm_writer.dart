import '../model/canvas.dart';
import 'writer_utils.dart';

extension CanvasPpmExtension on Canvas {
  String toPPMString() {
    const wrapCount = 70 - 4;
    final sb = StringBuffer();
    sb.writeln('P3');
    sb.writeln('$width $height');
    sb.writeln('255');
    for (int row = 0; row < height; row++) {
      final lb = StringBuffer();
      for (int column = 0; column < width; column++) {
        final p = getPixel(column, row);
        if (lb.length >= wrapCount) {
          sb.writeln(lb.toString());
          lb.clear();
        }
        lb.write('${colorValue(p.red)}');
        if (lb.length >= wrapCount) {
          sb.writeln(lb.toString());
          lb.clear();
        } else {
          lb.write(' ');
        }
        lb.write('${colorValue(p.green)}');
        if (lb.length >= wrapCount) {
          sb.writeln(lb.toString());
          lb.clear();
        } else {
          lb.write(' ');
        }
        lb.write('${colorValue(p.blue)}');
        if (column != width - 1) {
          lb.write(' ');
        }
      }
      sb.writeln(lb.toString());
    }

    return sb.toString();
  }
}
