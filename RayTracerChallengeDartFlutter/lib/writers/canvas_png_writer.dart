import 'dart:io';

import 'package:image/image.dart' as img;

import '../model/canvas.dart';
import 'writer_utils.dart';

extension CanvasPngWriter on Canvas {
  Future<void> writePng(File file) async {
    final iw = img.Image(width: width, height: height);
    for (int i = 0; i < height; i++) {
      for (int j = 0; j < width; j++) {
        final pixel = getPixel(j, i);
        iw.data!.setPixelRgb(j, i, colorValue(pixel.red),
            colorValue(pixel.green), colorValue(pixel.blue));
      }
    }
    final png = img.encodePng(iw);
    await file.writeAsBytes(png);
  }
}
