import 'package:buffer_image/buffer_image.dart';
import 'package:flutter/material.dart' as m;

import '../model/canvas.dart';
import 'writer_utils.dart';

extension BufferImageWriterExtension on Canvas {
  BufferImage buildBufferImage() {
    final image = BufferImage(width, height);
    for (int row = 0; row < height; row++) {
      for (int column = 0; column < width; column++) {
        final p = getPixel(column, row);
        final r = colorValue(p.red);
        final g = colorValue(p.green);
        final b = colorValue(p.blue);
        final c = m.Color.fromRGBO(r, g, b, 1.0);
        image.setColor(column, row, c);
      }
    }
    return image;
  }
}
