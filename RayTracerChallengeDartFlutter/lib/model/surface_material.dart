import '../utils.dart';
import 'color.dart';
import 'constants.dart';

const _minADS = 0.0;
const _minShininess = 10.0;
const _maxADS = 1.0;
const _maxShininess = 200.0;

SurfaceMaterial material({
  Color color = WHITE,
  num ambient = 0.1,
  num diffuse = 0.9,
  num specular = 0.9,
  num shininess = 200.0,
}) {
  if (ambient < _minADS || ambient > _maxADS) {
    throw Exception('Ambient setting is out of range: $ambient');
  }

  if (diffuse < _minADS || diffuse > _maxADS) {
    throw Exception('Diffuse setting is out of range: $diffuse');
  }

  if (specular < _minADS || specular > _maxADS) {
    throw Exception('Specular setting is out of range: $specular');
  }

  if (shininess < _minShininess || shininess > _maxShininess) {
    throw Exception('Shininess setting is out of range: $shininess');
  }

  return SurfaceMaterial(
    color: color,
    ambient: ambient,
    diffuse: diffuse,
    specular: specular,
    shininess: shininess,
  );
}

class SurfaceMaterial {
  final Color color;
  final num ambient;
  final num diffuse;
  final num specular;
  final num shininess;

  const SurfaceMaterial({
    required this.color,
    required this.ambient,
    required this.diffuse,
    required this.specular,
    required this.shininess,
  });

  SurfaceMaterial copyWith({
    Color? color,
    num? ambient,
    num? diffuse,
    num? specular,
    num? shininess,
  }) =>
      SurfaceMaterial(
        color: color ?? this.color,
        ambient: ambient ?? this.ambient,
        diffuse: diffuse ?? this.diffuse,
        specular: specular ?? this.specular,
        shininess: shininess ?? this.shininess,
      );

  @override
  bool operator ==(Object other) {
    if (other is! SurfaceMaterial) {
      return false;
    }

    return equals(other);
  }

  bool equals(SurfaceMaterial other, {double tolerance = epsilon}) {
    if (!color.equals(other.color, tolerance: tolerance)) {
      return false;
    }

    if (!ambient.equals(other.ambient, tolerance)) {
      return false;
    }
    if (!diffuse.equals(other.diffuse, tolerance)) {
      return false;
    }
    if (!specular.equals(other.specular, tolerance)) {
      return false;
    }
    if (!shininess.equals(other.shininess, tolerance)) {
      return false;
    }

    return true;
  }
}
