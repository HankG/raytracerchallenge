import 'dart:math';

import '../utils.dart';
import 'constants.dart';

Tuple point(num x, num y, num z) => Tuple(x, y, z, 1.0);

Tuple vector(num x, num y, num z) => Tuple(x, y, z, 0.0);

class Tuple {
  static const ZERO = Tuple(0.0, 0.0, 0.0, 0.0);

  final num x;
  final num y;
  final num z;
  final num w;

  bool get isPoint => (w - 1.0).abs() <= epsilon;

  bool get isVector => w.abs() <= epsilon;

  const Tuple(this.x, this.y, this.z, this.w);

  Tuple operator +(Tuple other) {
    return Tuple(x + other.x, y + other.y, z + other.z, w + other.w);
  }

  Tuple operator -(Tuple other) {
    return Tuple(x - other.x, y - other.y, z - other.z, w - other.w);
  }

  Tuple operator *(num a) {
    return Tuple(a * x, a * y, a * z, a * w);
  }

  Tuple operator /(num a) {
    return Tuple(x / a, y / a, z / a, w / a);
  }

  Tuple operator -() {
    return Tuple(-x, -y, -z, -w);
  }

  num dot(Tuple b) {
    if (!isVector) {
      throw Exception('a tuple of dot product (a . b) is not a vector');
    }

    if (!b.isVector) {
      throw Exception('b tuple of dot product (a . b) is not a vector');
    }

    return x * b.x + y * b.y + z * b.z;
  }

  Tuple cross(Tuple b) {
    if (!isVector) {
      throw Exception('a tuple of cross product (a x b) is not a vector');
    }

    if (!b.isVector) {
      throw Exception('b tuple of cross product (a x b) is not a vector');
    }
    final x = (this.y * b.z) - (this.z * b.y);
    final y = (this.z * b.x) - (this.x * b.z);
    final z = (this.x * b.y) - (this.y * b.x);
    return vector(x, y, z);
  }

  double get magnitude {
    if (!isVector) {
      throw Exception('tuple is not a vector');
    }

    return sqrt(x * x + y * y + z * z + w * w);
  }

  Tuple normalize() {
    double mag = magnitude;
    return Tuple(x / mag, y / mag, z / mag, w / mag);
  }

  @override
  bool operator ==(Object other) {
    if (other is! Tuple) {
      return false;
    }

    return equals(other);
  }

  @override
  int get hashCode => x.hashCode ^ y.hashCode ^ z.hashCode ^ w.hashCode;

  bool equals(Tuple other, {double tolerance = epsilon}) {
    if (!x.equals(other.x, tolerance)) {
      return false;
    }

    if (!y.equals(other.y, tolerance)) {
      return false;
    }

    if (!z.equals(other.z, tolerance)) {
      return false;
    }

    if (!w.equals(other.w, tolerance)) {
      return false;
    }

    return true;
  }

  @override
  String toString() => '[$x, $y, $z, $w]';
}
