import '../utils.dart';
import 'constants.dart';

Color color(num red, num green, num blue) => Color(red, green, blue);

const BLACK = Color(0, 0, 0);
const WHITE = Color(1, 1, 1);

class Color {
  final num red;
  final num green;
  final num blue;

  const Color(this.red, this.green, this.blue);

  Color operator +(Color other) => Color(
        red + other.red,
        green + other.green,
        blue + other.blue,
      );

  Color operator -(Color other) => Color(
        red - other.red,
        green - other.green,
        blue - other.blue,
      );

  Color operator *(Color other) => Color(
        red * other.red,
        green * other.green,
        blue * other.blue,
      );

  Color scale(num scalar) => Color(
        red * scalar,
        green * scalar,
        blue * scalar,
      );

  bool equals(Color other, {double tolerance = epsilon}) {
    if (!red.equals(other.red, tolerance)) {
      return false;
    }

    if (!green.equals(other.green, tolerance)) {
      return false;
    }

    if (!blue.equals(other.blue, tolerance)) {
      return false;
    }

    return true;
  }

  @override
  bool operator ==(Object other) => other is! Color ? false : equals(other);

  @override
  int get hashCode => red.hashCode ^ green.hashCode ^ blue.hashCode;

  @override
  String toString() {
    return 'Color{red: $red, green: $green, blue: $blue}';
  }
}
