import 'intersection.dart';
import 'matrix.dart';
import 'ray.dart';
import 'surface_material.dart';

abstract class Shape {
  late String id;
  late Matrix transform;
  late SurfaceMaterial surfaceMaterial;

  Shape({
    String idPrefix = '',
    String? initId,
    Matrix? initTransform,
    SurfaceMaterial? initMaterial,
  }) {
    final finalId = initId ?? DateTime.now().microsecondsSinceEpoch.toString();

    id = idPrefix.isEmpty ? finalId : '${idPrefix}_$finalId';
    transform = initTransform ?? Matrix.identity();
    surfaceMaterial = initMaterial ?? material();
  }

  Intersections intersect(Ray ray);

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is Shape && runtimeType == other.runtimeType && id == other.id;

  @override
  int get hashCode => id.hashCode;

  @override
  String toString() {
    return '${runtimeType}{id: $id}';
  }
}
