import 'package:raytracerchallenge/model/sphere.dart';

import 'shape.dart';

Intersection NO_INTERSECTION = Intersection(0, sphere());

class Intersection {
  final num t;
  final Shape object;

  Intersection(this.t, this.object);

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is Intersection &&
          runtimeType == other.runtimeType &&
          t == other.t &&
          object == other.object;

  @override
  int get hashCode => t.hashCode ^ object.hashCode;

  @override
  String toString() {
    return 'Intersection{t: $t, object: $object}';
  }
}

class Intersections {
  final List<Intersection> xs;

  int get count => xs.length;

  const Intersections(this.xs);

  factory Intersections.none() => const Intersections([]);

  Intersection hit() {
    if (xs.isEmpty) {
      return NO_INTERSECTION;
    }

    final potentialResults = xs.where((i) => i.t > 0).toList();
    if (potentialResults.isEmpty) {
      return NO_INTERSECTION;
    }

    potentialResults.sort((i1, i2) => i1.t.compareTo(i2.t));
    return potentialResults.first;
  }
}
