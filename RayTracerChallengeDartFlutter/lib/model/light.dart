import 'color.dart';
import 'tuple.dart';

Light pointLight(Color color, Tuple position) {
  if (!position.isPoint) {
    throw Exception('Position must be a point');
  }

  return Light(color, position);
}

class Light {
  final Color intensity;
  final Tuple position;

  Light(this.intensity, this.position);
}
