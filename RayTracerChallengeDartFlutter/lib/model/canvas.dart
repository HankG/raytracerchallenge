import 'color.dart';

class Canvas {
  final int width;
  final int height;
  late List<Color> _pixels;

  Canvas(this.width, this.height, {Color fillColor = BLACK}) {
    _pixels = List.generate(width * height, (index) => fillColor);
  }

  void fill(Color color) {
    for (int row = 0; row < height; row++) {
      for (int column = 0; column < width; column++) {
        setPixel(column, row, color);
      }
    }
  }

  void setPixel(int column, int row, Color color) {
    _pixels[_getIndex(column, row)] = color;
  }

  Color getPixel(int column, int row) {
    return _pixels[_getIndex(column, row)];
  }

  int _getIndex(int column, int row) {
    if (row < 0 || row >= height) {
      throw Exception('Row $row out of bounds 0 <= row < $height');
    }

    if (column < 0 || column >= width) {
      throw Exception('Column $column out of bounds 0 <= column < $width');
    }

    return row * width + column;
  }
}
