import 'dart:math';

import 'intersection.dart';
import 'matrix.dart';
import 'ray.dart';
import 'shape.dart';
import 'surface_material.dart';
import 'tuple.dart';

Sphere sphere() => Sphere();

class Sphere extends Shape {
  Sphere({super.initTransform, super.initMaterial});

  Sphere updateTransform(Matrix newTransform) =>
      Sphere(initTransform: newTransform);

  Sphere updateMaterial(SurfaceMaterial material) =>
      Sphere(initTransform: transform, initMaterial: material);

  Tuple normal(Tuple worldPoint) {
    if (!worldPoint.isPoint) {
      throw Exception('Surface point needs to be a point tuple');
    }

    final objectPoint = transform.invert().times(worldPoint);
    final objectNormal = objectPoint - point(0, 0, 0);
    final worldNormal = transform.invert().transpose().times(objectNormal);
    final result = vector(worldNormal.x, worldNormal.y, worldNormal.z);

    return result.normalize();
  }

  @override
  Intersections intersect(Ray ray) {
    final rayInObjectSpace = ray.transform(transform.invert());
    final rayToSphere = rayInObjectSpace.origin - point(0, 0, 0);
    final a = rayInObjectSpace.direction.dot(rayInObjectSpace.direction);
    final b = 2 * rayInObjectSpace.direction.dot(rayToSphere);
    final c = rayToSphere.dot(rayToSphere) - 1;
    final discriminate = (b * b) - (4 * a * c);
    if (discriminate < 0) {
      return Intersections.none();
    }
    final t1 = (-b - sqrt(discriminate)) / (2 * a);
    final t2 = (-b + sqrt(discriminate)) / (2 * a);
    return Intersections([
      Intersection(t1, this),
      Intersection(t2, this),
    ]);
  }
}
