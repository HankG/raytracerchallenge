import '../utils.dart';
import 'constants.dart';
import 'tuple.dart';

class Matrix {
  final int width;
  final int height;
  late final List<num> _elements;

  Matrix(this.width, this.height) {
    _elements = List.generate(width * height, (index) => 0);
  }

  factory Matrix.new2x2() => Matrix(2, 2);

  factory Matrix.new3x3() => Matrix(3, 3);

  factory Matrix.new4x4() => Matrix(4, 4);

  factory Matrix.identity({int size = 4}) {
    final result = Matrix(size, size);
    for (int i = 0; i < size; i++) {
      result.setValue(i, i, 1);
    }

    return result;
  }

  factory Matrix.initialized(List<List<num>> value) {
    if (value.isEmpty) {
      throw Exception("Can't initialize with empty array");
    }

    final height = value.length;
    final width = value.first.length;
    final result = Matrix(width, height);
    if (width == 0) {
      throw Exception("Can't initialize with empty rows");
    }

    for (int i = 0; i < height; i++) {
      if (value[i].length != width) {
        throw Exception("Can't initialize with irregular row width data");
      }
      for (int j = 0; j < width; j++) {
        result.setValue(i, j, value[i][j]);
      }
    }
    return result;
  }

  num getValue(int row, int column) => _elements[_getIndex(row, column)];

  num setValue(int row, int column, num value) =>
      _elements[_getIndex(row, column)] = value;

  bool get isInvertible => determinant().abs() > epsilon;

  List<List<num>> get value {
    final result = <List<num>>[];
    for (int i = 0; i < height; i++) {
      result.add(<num>[]);
      for (int j = 0; j < width; j++) {
        result[i].add(getValue(i, j));
      }
    }

    return result;
  }

  Matrix operator *(Matrix other) {
    if (width != 4 || height != 4) {
      throw Exception('Must be a 4x4 matrix: ($width x $height)');
    }

    if (other.width != 4 || other.height != 4) {
      throw Exception('Other matrix must be a 4x4 matrix: ($width x $height)');
    }

    final result = Matrix.new4x4();
    for (int row = 0; row < height; row++) {
      for (int column = 0; column < width; column++) {
        final value = getValue(row, 0) * other.getValue(0, column) +
            getValue(row, 1) * other.getValue(1, column) +
            getValue(row, 2) * other.getValue(2, column) +
            getValue(row, 3) * other.getValue(3, column);
        result.setValue(row, column, value);
      }
    }

    return result;
  }

  num cofactor(int row, int column) {
    if ((row + column) % 2 == 0) {
      return minor(row, column);
    }

    return -minor(row, column);
  }

  Matrix subMatrix(int rowToDelete, int columnToDelete) {
    final result = Matrix(width - 1, height - 1);

    int newRowIndex = 0;
    for (int row = 0; row < height; row++) {
      if (row == rowToDelete) {
        continue;
      }

      int newColumnIndex = 0;
      for (int column = 0; column < width; column++) {
        if (column == columnToDelete) {
          continue;
        }
        result.setValue(newRowIndex, newColumnIndex, getValue(row, column));
        newColumnIndex++;
      }
      newRowIndex++;
    }

    return result;
  }

  Tuple times(Tuple tuple) {
    if (width != 4 || height != 4) {
      throw Exception('Must be a 4x4 matrix: ($width x $height)');
    }

    final result = List<num>.generate(4, (index) => 0);
    for (int row = 0; row < height; row++) {
      final value = getValue(row, 0) * tuple.x +
          getValue(row, 1) * tuple.y +
          getValue(row, 2) * tuple.z +
          getValue(row, 3) * tuple.w;
      result[row] = value;
    }

    return Tuple(
      result[0],
      result[1],
      result[2],
      result[3],
    );
  }

  Matrix transpose() {
    final result = Matrix(height, width);
    for (int row = 0; row < height; row++) {
      for (int column = 0; column < width; column++) {
        result.setValue(column, row, getValue(row, column));
      }
    }
    return result;
  }

  Matrix invert() {
    if (width != height) {
      throw Exception('Only know how to invert a square matrix');
    }

    if (!isInvertible) {
      throw Exception("This matrix can't be inverted");
    }

    final result = Matrix(width, height);
    final det = determinant();
    for (int row = 0; row < height; row++) {
      for (int column = 0; column < width; column++) {
        final c = cofactor(row, column);
        result.setValue(column, row, c / det);
      }
    }

    return result;
  }

  num determinant() {
    if (width != height) {
      throw Exception(
        'Can only compute for a square matrices: $width x $height',
      );
    }

    if (width == 2 && height == 2) {
      return getValue(0, 0) * getValue(1, 1) - getValue(0, 1) * getValue(1, 0);
    }

    num result = 0.0;
    for (int column = 0; column < width; column++) {
      result += getValue(0, column) * cofactor(0, column);
    }

    return result;
  }

  num minor(int row, int column) {
    return subMatrix(row, column).determinant();
  }

  bool equals(Matrix other, {double tolerance = epsilon}) {
    if (width != other.width) {
      return false;
    }

    if (height != other.height) {
      return false;
    }

    if (_elements.length != other._elements.length) {
      return false;
    }

    for (int i = 0; i < _elements.length; i++) {
      if (!_elements[i].equals(other._elements[i], tolerance)) {
        return false;
      }
    }

    return true;
  }

  @override
  bool operator ==(Object other) {
    if (other is! Matrix) {
      return false;
    }
    return equals(other);
  }

  @override
  int get hashCode => width.hashCode ^ height.hashCode ^ _elements.hashCode;

  @override
  String toString() {
    final sb = StringBuffer();
    for (int row = 0; row < height; row++) {
      final elements = <num>[];
      for (int column = 0; column < width; column++) {
        elements.add(getValue(row, column));
      }
      sb.write('| ');
      sb.write(elements.join(' | '));
      sb.writeln(' |');
    }

    return sb.toString();
  }

  int _getIndex(int row, int column) {
    if (row < 0 || row >= height) {
      throw Exception('Row $row out of bounds 0 <= row < $height');
    }

    if (column < 0 || column >= width) {
      throw Exception('Column $column out of bounds 0 <= column < $width');
    }

    return row * width + column;
  }
}
