import 'matrix.dart';
import 'tuple.dart';

Ray ray(Tuple origin, Tuple direction) {
  if (!origin.isPoint) {
    throw Exception('Origin must be a point not a vector');
  }

  if (!direction.isVector) {
    throw Exception('Direction must be a vector not a point');
  }
  return Ray(origin, direction);
}

class Ray {
  final Tuple origin;
  final Tuple direction;

  const Ray(this.origin, this.direction);

  Tuple position(num t) => origin + direction * t;

  Ray transform(Matrix m) {
    final newOrigin = m.times(origin);
    final newDirection = m.times(direction);
    return Ray(newOrigin, newDirection);
  }
}
