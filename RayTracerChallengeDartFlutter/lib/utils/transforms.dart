import 'dart:math';

import 'package:raytracerchallenge/model/matrix.dart';
import 'package:raytracerchallenge/model/tuple.dart';

Matrix translate(num x, num y, num z) => Matrix.initialized([
      [1, 0, 0, x],
      [0, 1, 0, y],
      [0, 0, 1, z],
      [0, 0, 0, 1],
    ]);

Matrix scale(num x, num y, num z) => Matrix.initialized([
      [x, 0, 0, 0],
      [0, y, 0, 0],
      [0, 0, z, 0],
      [0, 0, 0, 1],
    ]);

Matrix shear(num xy, num xz, num yx, num yz, num zx, num zy) =>
    Matrix.initialized([
      [1, xy, xz, 0],
      [yx, 1, yz, 0],
      [zx, zy, 1, 0],
      [0, 0, 0, 1],
    ]);

Matrix rotateX(num radians) => Matrix.initialized([
      [1, 0, 0, 0],
      [0, cos(radians), -sin(radians), 0],
      [0, sin(radians), cos(radians), 0],
      [0, 0, 0, 1],
    ]);

Matrix rotateY(num radians) => Matrix.initialized([
      [cos(radians), 0, sin(radians), 0],
      [0, 1, 0, 0],
      [-sin(radians), 0, cos(radians), 0],
      [0, 0, 0, 1],
    ]);

Matrix rotateZ(num radians) => Matrix.initialized([
      [cos(radians), -sin(radians), 0, 0],
      [sin(radians), cos(radians), 0, 0],
      [0, 0, 1, 0],
      [0, 0, 0, 1],
    ]);

extension TupleTransforms on Tuple {
  Tuple reflect(Tuple normal) {
    if (!isVector) {
      throw Exception('Can only reflect a vector');
    }

    if (!normal.isVector) {
      throw Exception('Can only reflect with a normal vector');
    }

    return this - normal * (2 * dot(normal));
  }
}
