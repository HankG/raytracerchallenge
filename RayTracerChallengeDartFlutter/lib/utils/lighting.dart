import 'dart:math';

import 'package:raytracerchallenge/utils/transforms.dart';

import '../model/color.dart';
import '../model/light.dart';
import '../model/surface_material.dart';
import '../model/tuple.dart';

Color lighting(SurfaceMaterial material, Light light, Tuple point,
    Tuple eyeVector, Tuple normalVector) {
  if (!point.isPoint) {
    throw Exception('Point needs to be a point type');
  }

  if (!eyeVector.isVector) {
    throw Exception('Eye vector needs to be a vector type');
  }

  if (!normalVector.isVector) {
    throw Exception('Normal vector needs to be a vector type');
  }

  final effectiveColor = material.color * light.intensity;
  final lightVector = (light.position - point).normalize();

  final ambientColor = effectiveColor.scale(material.ambient);

  final ldn = lightVector.dot(normalVector);
  late final Color diffuseColor;
  late final Color specularColor;
  if (ldn < 0) {
    diffuseColor = BLACK;
    specularColor = BLACK;
  } else {
    diffuseColor = effectiveColor.scale(material.diffuse * ldn);
    final reflectionVector = (-lightVector).reflect(normalVector);
    final rvde = reflectionVector.dot(eyeVector);
    if (rvde <= 0) {
      specularColor = BLACK;
    } else {
      final factor = pow(rvde, material.shininess);
      specularColor = light.intensity.scale(material.specular * factor);
    }
  }

  final totalColor = ambientColor + diffuseColor + specularColor;
  return totalColor;
}
