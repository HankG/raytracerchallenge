import 'dart:io';

import 'package:file_picker/file_picker.dart';
import 'package:flutter/material.dart';

import 'canvas_viewer_widget.dart';
import 'model/canvas.dart' as rtc;
import 'model/color.dart' as c;
import 'scenarios/phong_shaded_sphere.dart';
import 'scenarios/pixel_stream_interface.dart';
import 'writers/canvas_png_writer.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Flutter Demo',
      theme: ThemeData(
        colorScheme: ColorScheme.fromSeed(seedColor: Colors.deepPurple),
        useMaterial3: true,
      ),
      home: const MyHomePage(title: 'Raytracer Challenge'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({super.key, required this.title});

  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

const canvasSize = 900;

class _MyHomePageState extends State<MyHomePage> {
  bool filling = false;
  rtc.Canvas canvas = rtc.Canvas(canvasSize, canvasSize);
  final IPixelStreamGenerator generator =
      const PhongShadedSphereScene(canvasSize: canvasSize);

  @override
  void initState() {
    super.initState();
    fill();
  }

  void _refresh() {
    setState(() {
      canvas.fill(c.BLACK);
    });
    fill();
  }

  void fill() async {
    if (filling) {
      print('fill running, skipping');
      return;
    }

    filling = true;
    final st = Stopwatch()..start();
    await for (final update in generator.generate()) {
      final column = update.x;
      final row = update.y;
      final color = update.color;
      canvas.setPixel(column, row, color);
      if (st.elapsedMilliseconds > 1000) {
        setState(() {});
        //await Future.delayed(const Duration(milliseconds: 100));
        st.reset();
      }
    }
    print('fill complete');
    filling = false;
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Theme.of(context).colorScheme.inversePrimary,
        title: Text(widget.title),
        actions: [
          IconButton(
              onPressed: () async {
                final file = await FilePicker.platform.saveFile(
                  dialogTitle: 'Save Output to...',
                  fileName: 'scene.png',
                  allowedExtensions: ['png'],
                );
                if (file == null) {
                  return;
                }
                await canvas.writePng(File(file));
              },
              icon: const Icon(Icons.save))
        ],
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Expanded(child: CanvasViewerWidget(canvas: canvas)),
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: _refresh,
        tooltip: 'Refresh',
        child: const Icon(Icons.refresh),
      ), // This trailing comma makes auto-formatting nicer for build methods.
    );
  }
}
