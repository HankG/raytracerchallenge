import 'dart:math';

extension NumberExtensions on num {
  bool equals(num other, double tolerance) {
    return (toDouble() - other.toDouble()).abs() <= tolerance;
  }

  num toRadians() => this * pi / 180.0;

  num toDegrees() => this * 180.0 / pi;
}
