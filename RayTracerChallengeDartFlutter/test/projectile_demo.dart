import 'package:flutter_test/flutter_test.dart';
import 'package:raytracerchallenge/model/tuple.dart';

class Projectile {
  final Tuple p;
  final Tuple v;

  const Projectile(this.p, this.v);

  Projectile update(Tuple gravity, Tuple wind) {
    final p2 = p + v;
    final v2 = v + gravity + wind;
    return Projectile(p2, v2);
  }

  String toString() => '${p.x},${p.y},${p.z},${v.x},${v.y},${v.z}';
}

void main() {
  test('Demo Tuples with projectile test', () {
    print('Demo');
    final gravity = vector(0, -0.1, 0);
    final wind = vector(0.01, 0, 0);
    var projectile = Projectile(
      point(0, 1, 0),
      vector(1, 1, 0),
    );
    while (projectile.p.y >= 0) {
      print(projectile);
      projectile = projectile.update(gravity, wind);
    }
  });
}
